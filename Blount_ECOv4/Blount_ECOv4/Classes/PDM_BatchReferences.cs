﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EPDM.Interop.epdm;
using System.Windows.Forms;

namespace Blount_ECOv4
{
    static class PDM_BatchReferences
    {
        public static IEdmVault5 Vault { get; set; }
        public static int TransitionID { get; set; }

        public static string[] GetReferences(this IEdmFile5 file, IEdmFolder5 folder)
        {
            Vault = file.Vault;
            IEdmReference5 reference = file.GetReferenceTree(folder.ID);
            Dictionary<string, string> filesDictionary = new Dictionary<string, string>();
            GetReferencesRecursively(reference, file.GetLocalPath(folder.ID), 0, "W", ref filesDictionary);

            var references = new List<string>();
            foreach (var item in filesDictionary)
                references.Add(item.Key);

            return references.ToArray();
        }

        private static void GetReferencesRecursively(IEdmReference5 Reference, string FilePath, int Level, string ProjectName, ref Dictionary<string, string> FilesDictionary)
        {
            if (Vault == null)
                throw new NullReferenceException("Vault property is null.");
            bool Top = false;
            if (Reference == null)
            {
                //This is the first time this function is called for this reference tree; i.e., this is the root
                Top = true;
                //Add the top-level file path to the dictionary RefFilesDictionary.Add(FilePath, Level.ToString());
                IEdmFile5 File = null;
                IEdmFolder5 ParentFolder = null;

                File = Vault.GetFileFromPath(FilePath, out ParentFolder);
                //Get the reference tree for this file
                Reference = File.GetReferenceTree(ParentFolder.ID);
                GetReferencesRecursively(Reference, "", Level + 1, ProjectName, ref FilesDictionary);
            }
            else //Execute this code when this function is called recursively; i.e., this is not the top-level IEdmReference in the tree
            {


                //Recursively traverse the references
                IEdmPos5 pos = default(IEdmPos5);
                IEdmReference9 Reference2 = (IEdmReference9)Reference;
                pos = Reference2.GetFirstChildPosition3(ProjectName, Top, true, (int)EdmRefFlags.EdmRef_File, "", 0);
                IEdmReference5 @ref = default(IEdmReference5);

                while ((!pos.IsNull))
                {
                    @ref = Reference.GetNextChild(pos);

                    // check if file exists 
                    //-JH 18.01.000 08.08.2018(added the dictionary.contains key)
                    if (System.IO.File.Exists(@ref.FoundPath) && FilesDictionary.ContainsKey(@ref.FoundPath)==false)
                    {
                        FilesDictionary.Add(@ref.FoundPath, Level.ToString());
                        GetReferencesRecursively(@ref, "", Level + 1, ProjectName, ref FilesDictionary);
                    }
                }
            }
        }

        public static bool Batch_Unlock(string[] files, int Handle)
        {
            try
            {
                //create batch unlocker object 
                var batchUnlocker = (IEdmBatchUnlock2)(Vault as IEdmVault8).CreateUtility(EdmUtility.EdmUtil_BatchUnlock) as IEdmBatchUnlock2;

                //create the selection list 
                var list = new List<EdmSelItem>();
                var selectedFile = default(EdmSelItem);

                foreach (var file in files)
                {
                    var ppoRetParentFolder = default(IEdmFolder5);
                    var aFile = Vault.GetFileFromPath(file, out ppoRetParentFolder);
                    selectedFile = new EdmSelItem();
                    IEdmPos5 aPos = aFile.GetFirstFolderPosition();
                    IEdmFolder5 aFolder = aFile.GetNextFolder(aPos);
                    selectedFile.mlDocID = aFile.ID;
                    selectedFile.mlProjID = aFolder.ID;
                    list.Add(selectedFile);
                }

                // do not change this - the array must init as null
                EdmSelItem[] ppoSelection = null;
                ppoSelection = list.ToArray();
                batchUnlocker.AddSelection((EdmVault5)Vault, ppoSelection);
                //create tree 
                batchUnlocker.CreateTree(Handle, (int)EdmUnlockBuildTreeFlags.Eubtf_ShowCloseAfterCheckinOption + (int)EdmUnlockBuildTreeFlags.Eubtf_MayUnlock);
                // unlock file 
                batchUnlocker.UnlockFiles(Handle, null);
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }

        }

        public static bool Batch_Lock(string[] files, int Handle)
        {
            try
            {
                //create batch unlocker object 
                IEdmVault7 vault7 = Vault as IEdmVault7;
                IEdmBatchGet batchLocker;
                batchLocker = (IEdmBatchGet)vault7.CreateUtility(EdmUtility.EdmUtil_BatchGet);

                //create the selection list 
                var list = new List<EdmSelItem>();
                var selectedFile = default(EdmSelItem);

                foreach (var file in files)
                {
                    var ppoRetParentFolder = default(IEdmFolder5);
                    var aFile = Vault.GetFileFromPath(file, out ppoRetParentFolder);
                    selectedFile = new EdmSelItem();
                    IEdmPos5 aPos = aFile.GetFirstFolderPosition();
                    IEdmFolder5 aFolder = aFile.GetNextFolder(aPos);
                    selectedFile.mlDocID = aFile.ID;
                    selectedFile.mlProjID = aFolder.ID;
                    list.Add(selectedFile);
                }

                //Do not change this - the array must init as null
                EdmSelItem[] ppoSelection = null;
                ppoSelection = list.ToArray();
                batchLocker.AddSelection((EdmVault5)Vault, ppoSelection);

                //Create tree 
                batchLocker.CreateTree(Handle, (int)EdmGetCmdFlags.Egcf_Lock);

                //Lock file 
                batchLocker.GetFiles(Handle, null);


                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }

        }

        public static bool Batch_UndoLock(string[] files, int Handle)
        {
            try
            {
                //create batch unlocker object 
                IEdmVault7 vault7 = Vault as IEdmVault7;
                IEdmBatchUnlock batchUnLocker;
                batchUnLocker = (IEdmBatchUnlock)vault7.CreateUtility(EdmUtility.EdmUtil_BatchUnlock);

                //create the selection list 
                var list = new List<EdmSelItem>();
                var selectedFile = default(EdmSelItem);

                foreach (var file in files)
                {
                    var ppoRetParentFolder = default(IEdmFolder5);
                    var aFile = Vault.GetFileFromPath(file, out ppoRetParentFolder);
                    selectedFile = new EdmSelItem();
                    IEdmPos5 aPos = aFile.GetFirstFolderPosition();
                    IEdmFolder5 aFolder = aFile.GetNextFolder(aPos);
                    selectedFile.mlDocID = aFile.ID;
                    selectedFile.mlProjID = aFolder.ID;
                    list.Add(selectedFile);
                }

                //Do not change this - the array must init as null
                EdmSelItem[] ppoSelection = null;
                ppoSelection = list.ToArray();
                batchUnLocker.AddSelection((EdmVault5)Vault, ppoSelection);


                //Create tree 
                batchUnLocker.CreateTree(Handle, (int)EdmGetCmdFlags.Egcf_Lock);

                //Lock file 
                batchUnLocker.UnlockFiles(Handle, null);

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }

        }

        public static void Batch_ChangeState(string[] files, string transitionName, int Handle)
        {
            try
            {
                // create batch   object 
                var batchChanger = (IEdmBatchChangeState)(Vault as IEdmVault8).CreateUtility(EdmUtility.EdmUtil_BatchChangeState) as IEdmBatchChangeState;
                IEdmFile5 PDMFile = default(IEdmFile5);
                IEdmFolder5 PDMFolder = default(IEdmFolder5);

                foreach (var file in files)
                {
                    PDMFile = Vault.GetFileFromPath(file, out PDMFolder);
                    batchChanger.AddFile(PDMFile.ID, PDMFolder.ID);
                }
                //create tree 
                batchChanger.CreateTree(transitionName);
                // change state 
                batchChanger.ChangeState(Handle, null);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


    }
}
