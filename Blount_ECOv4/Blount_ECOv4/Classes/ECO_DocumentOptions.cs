﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EPDM.Interop.epdm;
using System.Windows.Forms;

namespace Blount_ECOv4
{
    static class ECO_DocumentOptions
    {
        public static void Open(string CurrentDoc)
        {
            Globals.ThisAddIn.Application.Workbooks.Open(CurrentDoc);
        }

        public static void Close(string CurrentDoc)
        {
            Globals.ThisAddIn.Application.DisplayAlerts = false;
            Globals.ThisAddIn.Application.ActiveWorkbook.Close();
        }

        public static void Unlock(IEdmVault5 Vault, IEdmFolder5 Folder, IEdmFile5 File, string CurrentDoc, string UnlockMessage, int Handle)
        {
            try
            {
                File = Vault.GetFileFromPath(CurrentDoc, out Folder);

                if (File.IsLocked)
                {
                    File.UnlockFile(Handle, UnlockMessage, 0, null);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public static void Lock(IEdmVault5 Vault, IEdmFolder5 Folder, IEdmFile5 File, string CurrentDoc, int Handle)
        {
            File = Vault.GetFileFromPath(CurrentDoc, out Folder);
            IEdmUser6 currentUser = default(IEdmUser6);

            try
            {
                if (File.IsLocked)
                {
                    currentUser = (IEdmUser6)File.LockedByUser;
                    MessageBox.Show("Unable to process your request at this time. " + currentUser.FullName + " is currently processing this ECO");
                    return;
                }
                else
                {
                    File.GetFileCopy(Handle, null, Folder.ID, 0, "");
                    File.LockFile(Folder.ID, Handle, (int)EdmLockFlag.EdmLock_Simple);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public static void UndoLock(IEdmVault5 Vault, IEdmFolder5 Folder, IEdmFile5 File, string CurrentDoc, int Handle)
        {

            try
            {
                File = Vault.GetFileFromPath(CurrentDoc, out Folder);

                if (File.IsLocked)
                {
                    File.UndoLockFile(Handle, true);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public static void ChangeState(IEdmVault5 Vault, IEdmFolder5 Folder, IEdmFile5 File, string StateName, string CurrentDoc, int Handle)
        { 
            File = Vault.GetFileFromPath(CurrentDoc, out Folder);
            File.ChangeState(StateName, Folder.ID, "", Handle, 0);
        }
   }
}
