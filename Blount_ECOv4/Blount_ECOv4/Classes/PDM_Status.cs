﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EPDM.Interop.epdm;

namespace Blount_ECOv4
{
    class PDM_Status
    {
        internal static string SetStatusState(IEdmVault5 Vault, IEdmFolder5 Folder, IEdmFile5 File, IEdmState5 State, string CurrentDoc)
        {
            string state = null;
            File = Vault.GetFileFromPath(CurrentDoc, out Folder);
            State = File.CurrentState;
            state = State.Name;

            return state;
        }
        
        internal static string SetStatusOwner(IEdmVault5 Vault, IEdmFolder5 Folder, IEdmFile5 File, IEdmState5 State, string CurrentDoc)
        {
            string owner = null;
            if (File.IsLocked)
            {
                IEdmUser5 user5 = default(IEdmUser5);
                IEdmUser6 user = user5 as IEdmUser6;
                user = (IEdmUser6)File.LockedByUser;
                owner = user.FullName;
            }
            else
            {
                owner = "";
            }
            return owner;
        }


        internal static int SetVersion(IEdmFile5 File)
        { 
            return File.CurrentVersion;
        }
    }
}
