﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blount_ECOv4
{
    class XLS_RangeData
    {
        #region Private fields
        string _cellContent;
        #endregion

        #region Constructor
        public XLS_RangeData(string CellContent)
        {
            this._cellContent = CellContent;
        }
        #endregion


        public void SetCCBAppvl_FullName()
        {
            XLS_RangeEngine FullName = new XLS_RangeEngine(_cellContent,
                                                           "CCB.Appvl.FullName01",
                                                           "CCB.Appvl.FullName02",
                                                           "CCB.Appvl.FullName03",
                                                           "CCB.Appvl.FullName04",
                                                           "CCB.Appvl.FullName05",
                                                           "CCB.Appvl.FullName06",
                                                           "CCB.Appvl.FullName07",
                                                           "CCB.Appvl.FullName08",
                                                           "CCB.Appvl.FullName09",
                                                           "CCB.Appvl.FullName10");
            FullName.ArrayInput_Multi();

        }
        public void SetCCBAppvl_Title()
        {
            XLS_RangeEngine Title = new XLS_RangeEngine(_cellContent,
                                                           "CCB.Appvl.Title01",
                                                           "CCB.Appvl.Title02",
                                                           "CCB.Appvl.Title03",
                                                           "CCB.Appvl.Title04",
                                                           "CCB.Appvl.Title05",
                                                           "CCB.Appvl.Title06",
                                                           "CCB.Appvl.Title07",
                                                           "CCB.Appvl.Title08",
                                                           "CCB.Appvl.Title09",
                                                           "CCB.Appvl.Title10");
            Title.ArrayInput_Multi();
        }
        public void SetCCBAppvl_SysName()
        {
            XLS_RangeEngine SysName = new XLS_RangeEngine(_cellContent,
                                                           "CCB.Appvl.SysName01",
                                                           "CCB.Appvl.SysName02",
                                                           "CCB.Appvl.SysName03",
                                                           "CCB.Appvl.SysName04",
                                                           "CCB.Appvl.SysName05",
                                                           "CCB.Appvl.SysName06",
                                                           "CCB.Appvl.SysName07",
                                                           "CCB.Appvl.SysName08",
                                                           "CCB.Appvl.SysName09",
                                                           "CCB.Appvl.SysName10");
            SysName.ArrayInput_Multi();
        }

        public void SetCADAppvl_FullName()
        {
            XLS_RangeEngine FullName = new XLS_RangeEngine(_cellContent, "CAD.Appvl.FullName01",
                                                                        "CAD.Appvl.FullName02",
                                                                        "CAD.Appvl.FullName03",
                                                                        "CAD.Appvl.FullName04",
                                                                        "CAD.Appvl.FullName05",
                                                                        "CAD.Appvl.FullName06",
                                                                        "CAD.Appvl.FullName07",
                                                                        "CAD.Appvl.FullName08",
                                                                        "CAD.Appvl.FullName09",
                                                                        "CAD.Appvl.FullName10");
            FullName.ArrayInput_Multi();
        }
        public void SetCADAppvlSysName()
        {
            XLS_RangeEngine SysName = new XLS_RangeEngine(_cellContent, "CAD.Appvl.SysName01",
                                                                        "CAD.Appvl.SysName02",
                                                                        "CAD.Appvl.SysName03",
                                                                        "CAD.Appvl.SysName04",
                                                                        "CAD.Appvl.SysName05",
                                                                        "CAD.Appvl.SysName06",
                                                                        "CAD.Appvl.SysName07",
                                                                        "CAD.Appvl.SysName08",
                                                                        "CAD.Appvl.SysName09",
                                                                        "CAD.Appvl.SysName10");
            SysName.ArrayInput_Multi();
        }
        public void SetCADAppvl_Title()
        {
            XLS_RangeEngine Title = new XLS_RangeEngine(_cellContent, "CAD.Appvl.Title01",
                                                                      "CAD.Appvl.Title02",
                                                                      "CAD.Appvl.Title03",
                                                                      "CAD.Appvl.Title04",
                                                                      "CAD.Appvl.Title05",
                                                                      "CAD.Appvl.Title06",
                                                                      "CAD.Appvl.Title07",
                                                                      "CAD.Appvl.Title08",
                                                                      "CAD.Appvl.Title09",
                                                                      "CAD.Appvl.Title10");

            Title.ArrayInput_Multi();
        }

        public void SetNotify_FullName()
        {
            XLS_RangeEngine FullName = new XLS_RangeEngine(_cellContent,
                                                           "CCB.Notify.FullName01",
                                                           "CCB.Notify.FullName02",
                                                           "CCB.Notify.FullName03",
                                                           "CCB.Notify.FullName04",
                                                           "CCB.Notify.FullName05",
                                                           "CCB.Notify.FullName06",
                                                           "CCB.Notify.FullName07",
                                                           "CCB.Notify.FullName08",
                                                           "CCB.Notify.FullName09",
                                                           "CCB.Notify.FullName10");
            FullName.ArrayInput_Multi();
        }
        public void SetNotify_Title()
        {
            XLS_RangeEngine Title = new XLS_RangeEngine(_cellContent,
                                                           "CCB.Notify.Title01",
                                                           "CCB.Notify.Title02",
                                                           "CCB.Notify.Title03",
                                                           "CCB.Notify.Title04",
                                                           "CCB.Notify.Title05",
                                                           "CCB.Notify.Title06",
                                                           "CCB.Notify.Title07",
                                                           "CCB.Notify.Title08",
                                                           "CCB.Notify.Title09",
                                                           "CCB.Notify.Title10");
            Title.ArrayInput_Multi();
        }
        public void SetNotify_SysName()
        {
            XLS_RangeEngine SysName = new XLS_RangeEngine(_cellContent,
                                                           "CCB.Notify.SysName01",
                                                           "CCB.Notify.SysName02",
                                                           "CCB.Notify.SysName03",
                                                           "CCB.Notify.SysName04",
                                                           "CCB.Notify.SysName05",
                                                           "CCB.Notify.SysName06",
                                                           "CCB.Notify.SysName07",
                                                           "CCB.Notify.SysName08",
                                                           "CCB.Notify.SysName09",
                                                           "CCB.Notify.SysName10");
            SysName.ArrayInput_Multi();
        }

        public void SetVoteName()
        {
            XLS_RangeEngine voteName = new XLS_RangeEngine(_cellContent, "CCB.Vote01",
                                                                      "CCB.Vote02",
                                                                      "CCB.Vote03",
                                                                      "CCB.Vote04",
                                                                      "CCB.Vote05",
                                                                      "CCB.Vote06",
                                                                      "CCB.Vote07",
                                                                      "CCB.Vote08",
                                                                      "CCB.Vote09",
                                                                      "CCB.Vote10");
            voteName.ArrayInput_Multi();
        }
        public void SetVoteDate()
        {
            XLS_RangeEngine voteDate = new XLS_RangeEngine(_cellContent, "CCB.Vote.Date01",
                                                                      "CCB.Vote.Date02",
                                                                      "CCB.Vote.Date03",
                                                                      "CCB.Vote.Date04",
                                                                      "CCB.Vote.Date05",
                                                                      "CCB.Vote.Date06",
                                                                      "CCB.Vote.Date07",
                                                                      "CCB.Vote.Date08",
                                                                      "CCB.Vote.Date09",
                                                                      "CCB.Vote.Date10");
            voteDate.ArrayInput_Multi();
        }

        public void SetLocation()
        {
            XLS_RangeEngine location = new XLS_RangeEngine(_cellContent, "CCB.Location01",
                                                                         "CCB.Location02",
                                                                         "CCB.Location03",
                                                                         "CCB.Location04",
                                                                         "CCB.Location05",
                                                                         "CCB.Location06",
                                                                         "CCB.Location07");
            location.ArrayInput_Multi();
        }

        public void Disposition()
        { 
            XLS_RangeEngine dispositon = new XLS_RangeEngine(_cellContent, "CCB.Disposition");
            dispositon.ArrayInput_Single();
        }
     
        public void SetAssignedTo_FullName()
        {
            XLS_RangeEngine assignedTo = new XLS_RangeEngine(_cellContent, "CCB.AssignedTo.FullName");
            assignedTo.ArrayInput_Single();
        }

        public void SetAssignedTo_SysName()
        {
            XLS_RangeEngine assignedTo_SysName = new XLS_RangeEngine(_cellContent, "CCB.AssignedTo.SysName");
            assignedTo_SysName.ArrayInput_Single();
        }
      
        public void SetRequestedBy_FullName()
        {
            XLS_RangeEngine requestedBy = new XLS_RangeEngine(_cellContent, "CCB.ReqstBy.FullName");
            requestedBy.ArrayInput_Single();
        }

        public void SetRequestBy_SysName()
        {
            XLS_RangeEngine requstedBy_SysName = new XLS_RangeEngine(_cellContent, "CCB.ReqstBy.SysName");
            requstedBy_SysName.ArrayInput_Single();
        }
            
        public void SetDivision()
        {
            XLS_RangeEngine division = new XLS_RangeEngine(_cellContent, "CCB.Division");
            division.ArrayInput_Single();
        }
          
        public void SetCategory()
        {
            XLS_RangeEngine category = new XLS_RangeEngine(_cellContent, "CCB.Category");
            category.ArrayInput_Single();
        }
            
        public void SetPriority()
        {
            XLS_RangeEngine pritority = new XLS_RangeEngine(_cellContent, "CCB.Priority");
            pritority.ArrayInput_Single();
        }
      
        public void SetProject()
        {
            XLS_RangeEngine project = new XLS_RangeEngine(_cellContent, "CCB.ProjectType");
            project.ArrayInput_Single();
        }
    
        public void SetECOnumber()
        {
            XLS_RangeEngine eco = new XLS_RangeEngine(_cellContent, "ECOnumber");
            eco.ArrayInput_Single();
        }
      
    }

}

