﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blount_ECOv4
{
    internal static class SQL_VariableFinder
    {
        /// <summary>
        /// Seatch for the variable name with the variable enumns
        /// </summary>
        internal static string FetchVariableName(int variableID)
        {

            DataClasses1DataContext dB = new DataClasses1DataContext();

            var variable = (from v in dB.Variables
                             where v.VariableID.Equals(variableID)
                             select v.VariableName).Single();

            return variable;
        }
    }
}
