﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EPDM.Interop.epdm;

namespace Blount_ECOv4
{
    public static class PDM_LoggedInUser
    {
       
       static IEdmUser5 user5 = default(IEdmUser5);
       static IEdmUserMgr5 userMgr5 = default(IEdmUserMgr5);


        internal static string UserName(IEdmVault5 Vault)
        {
            //casting 
            IEdmUser6 user6 = user5 as IEdmUser6;

            userMgr5 = (IEdmUserMgr5)Vault;
            user6 = (IEdmUser6)userMgr5.GetLoggedInUser();

            return user6.Name;
        } 

        internal static string FullName(IEdmVault5 Vault)
        {
            IEdmUser6 user = user5 as IEdmUser6;
            userMgr5 = (IEdmUserMgr5)Vault;
            user = (IEdmUser6)userMgr5.GetLoggedInUser();

            return user.FullName;
        }
    }
}
