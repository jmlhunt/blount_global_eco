﻿using Microsoft.Office.Interop.Excel;
using System.Windows.Forms;
using System.Collections.Generic;
using System;
using Excel = Microsoft.Office.Interop.Excel;

namespace Blount_ECOv4
{
    class XLS_RangeEngine
    {
        #region Private Fields
        string _cellContent, _cell0, _cell1, _cell2, _cell3, _cell4, _cell5, _cell6, _cell7, _cell8, _cell9;
        string errorMsg = "Unable to locate the corresponding range name. Please use the Global ECO form for this control";

        #endregion

        #region Constructor: Location
        public XLS_RangeEngine(string CellContent, string Cell0, string Cell1, string Cell2, string Cell3, string Cell4,
                               string Cell5, string Cell6)
        {
            this._cellContent = CellContent;
            this._cell0 = Cell0;
            this._cell1 = Cell1;
            this._cell2 = Cell2;
            this._cell3 = Cell3;
            this._cell4 = Cell4;
            this._cell5 = Cell5;
            this._cell6 = Cell6;
            
        }
        #endregion

        #region Constructor: Approverrs and Notifications 
        public XLS_RangeEngine(string CellContent, string Cell0, string Cell1, string Cell2, string Cell3,
                               string Cell4, string Cell5, string Cell6, string Cell7, string Cell8, string Cell9)
        {
            this._cellContent = CellContent;
            this._cell0 = Cell0;
            this._cell1 = Cell1;
            this._cell2 = Cell2;
            this._cell3 = Cell3;
            this._cell4 = Cell4;
            this._cell5 = Cell5;
            this._cell6 = Cell6;
            this._cell7 = Cell7;
            this._cell8 = Cell8;
            this._cell9 = Cell9;
        }
        #endregion

        #region Muti Entry
        public void ArrayInput_Multi()
        {
            List<object> cells = new List<object>();
            cells.Add(_cell0);
            cells.Add(_cell1);
            cells.Add(_cell2);
            cells.Add(_cell3);
            cells.Add(_cell4);
            cells.Add(_cell5);
            cells.Add(_cell6);
            cells.Add(_cell7);
            cells.Add(_cell8);
            cells.Add(_cell9);

            try
            {
                foreach (var cell in cells)
                {
                    Worksheet currentSheet = Globals.ThisAddIn.GetActiveWorkSheet();

                    string cellval = currentSheet.Range[cell].Value;

                    if (string.IsNullOrWhiteSpace(cellval))
                    {
                        currentSheet.Range[cell].Value = _cellContent;
                        return;
                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message + " " + errorMsg);
            }

        }
        #endregion

        #region single Entry
        public XLS_RangeEngine(string CellContent, string Cell0)
        {
            this._cellContent = CellContent;
            this._cell0 = Cell0;
        }     

        public void ArrayInput_Single()
        {
            try
            {
                Worksheet currentWorksheet = Globals.ThisAddIn.GetActiveWorkSheet();
                currentWorksheet.Range[_cell0].Value = _cellContent;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message + " " + errorMsg);
                
            }
        }
        #endregion
    }
}

