﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blount_ECOv4
{
    static class SQL_StatusFinder
    {
        /// <summary>
        /// Search for the state name with the statusID emum.
        /// </summary>
        internal static string FetchStatusName(int StatusID)
        {
            DataClasses1DataContext db = new DataClasses1DataContext();

            var statusName = (from s in db.Status
                              where s.StatusID.Equals(StatusID)
                              select s.Name).First();

            return statusName;
        }
    }
}
