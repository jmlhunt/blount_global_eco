﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EPDM.Interop.epdm;
using System.IO;

namespace Blount_ECOv4
{

    public partial class Form_TransitionComment : Form
    {

        #region Inirialize vault objects
        Form form = new Form();
        IEdmState5 state = default(IEdmState5);
        IEdmEnumeratorVariable5 enumVar5 = default(IEdmEnumeratorVariable5);
        IEdmState5 currentState = default(IEdmState5);
        DataClasses1DataContext db = new DataClasses1DataContext();
       

        public delegate void testDelegate();
        public testDelegate doWorkOnIU;
        


        //public int ChangeStateID { get; set; }
        public int TransitionID { get; set; }
        public int StatusID { get; set; }
        public int TransSwitch { get; set; }
        public string Stamp_Approval { get; set; }
        public string MessageToUser_Success { get; set; }
        public string MessageToUser_Failure { get; set; }
        public Enum_ApprvlType ApprvlType { get; set; }


        //Private Fields
        private string _root_SysName;
        private string _root_Vote;
        private string _root_VoteDate;
        private string _root_Comment;
        private string _currentDoc;
        private IEdmVault5 _vault;
        private IEdmFolder5 _folder;
        private IEdmFile5 _file;

        
        
        private string unlockMessage = "";

        //Constructor
        public Form_TransitionComment(IEdmVault5 Vault, IEdmFolder5 Folder, IEdmFile5 File, string CurrentDoc, string Root_SysName, string Root_Vote, string Root_VoteDate, string Root_Comment)
        {
            InitializeComponent();
            this._vault = Vault;
            this._folder = Folder;
            this._file = File;
            this._currentDoc = CurrentDoc;
            this._root_SysName = Root_SysName;
            this._root_Vote = Root_Vote;
            this._root_VoteDate = Root_VoteDate;
            this._root_Comment = Root_Comment;
        }
        #endregion

        #region Initialize Form
        private void Form_Comment_Load(object sender, EventArgs e)
        {  
            this.Text = "Notification comment for " + _file.Name;
            lbl_State.Text = PDM_Status.SetStatusState(_vault, _folder, _file, state, _currentDoc);
            lbl_Version.Text = PDM_Status.SetVersion(_file).ToString();
            lbl_Report.Text = "";
        }
        #endregion

        #region Button: Submit (to apply approval comment to ECO file ONLY)
        private void btn_Submit_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            IEdmEnumeratorVariable10 enumVar10 = enumVar5 as IEdmEnumeratorVariable10;
            enumVar10 = (IEdmEnumeratorVariable10)_file.GetEnumeratorVariable();

            btn_Cancel.Enabled = false;
            btn_Clear.Enabled = false;
            txtBx_Comment.Enabled = false;
            btn_Submit.Enabled = false;
           
            #region Method Initialize
            _file = _vault.GetFileFromPath(_currentDoc, out _folder);
            string vaultUserName = PDM_LoggedInUser.UserName(_vault);
            DateTime date = DateTime.Now;
            string Stamp_ApprovalDate = date.ToString("MM.dd.yyyy");

            PDM_BatchReferences.Vault = _vault;
            string[] references = _file.GetReferences(_folder);

            object RetVote;
            object retSysName;
            int countSysName = 0;
            int countVote = 0;
            #endregion

            #region Count Approvers
            try
            {
                lbl_Report.Text = "Collecting approvers";

                for (int i = 1; i <= 10; i++)
                {
                    enumVar10.GetVar2(_root_SysName + i.ToString("00"), "", _folder.ID, out retSysName);

                    if (!string.IsNullOrEmpty((string)retSysName))
                    {
                        countSysName = countSysName + 1;
                    }
                }
                enumVar10.CloseFile(true);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            #endregion

          
            if (countSysName >= 1)
            {
                try
                {

                    //Close ECO
                    lbl_Report.Text = "Closing the active ECO document";
                    ECO_DocumentOptions.Close(_currentDoc);


                    //Lock ECO files
                    lbl_Report.Text = "Locking the active ECO document";
                    ECO_DocumentOptions.Lock(_vault, _folder, _file, _currentDoc, form.Handle.ToInt32());


                    //Comment
                    lbl_Report.Text = "Adding comment to email notification";
                    Comment();

                    //Reset ECO transition switch
                    lbl_Report.Text = "Preparing notification email";
                    ResetTranSwitch(_currentDoc);

                    //Set ECO transition Switch
                    lbl_Report.Text = "Sending notification email to all approvers";
                    SetTransitionSwitch(_currentDoc);

                    //Reset and Set references transition switch
                    if (references.Count() == 0)
                    {
                        lbl_Report.Text = "";
                    }
                    else
                    {
                        lbl_Report.Text = "Locking all affected items";
                        PDM_BatchReferences.Batch_Lock(references, form.Handle.ToInt32());
                        foreach (string reference in references)
                        {
                            //Reset
                            ResetTranSwitch(reference);

                            //Set
                            SetTransitionSwitch(reference);
                        }
                    }

                    //Conditionals
                    #region (IF) Send to Approvers
                    lbl_Report.Text = "Sending ECO document to approvers";
                    if (ApprvlType == Enum_ApprvlType.SendtoApprovers)
                    {

                        //Clear all approval stamps

                        _file = _vault.GetFileFromPath(_currentDoc, out _folder);
                        enumVar10 = (IEdmEnumeratorVariable10)_file.GetEnumeratorVariable();

                        for (int i = 1; i <= 10; i++)
                        {
                            enumVar10.SetVar(_root_Vote + i.ToString("00"), "", "", true);
                            enumVar10.SetVar(_root_VoteDate + i.ToString("00"), "", "", true);
                            enumVar10.SetVar(_root_Comment + i.ToString("00"), "", "", true);
                        }
                        enumVar10.CloseFile(true);
                    }
                    #endregion

                    #region (IF) Approval  - Apply approval Stamp
                    lbl_Report.Text = "Updating approval stamp";
                    if (ApprvlType == Enum_ApprvlType.Approved)
                    {
                        _file = _vault.GetFileFromPath(_currentDoc, out _folder);
                        enumVar10 = (IEdmEnumeratorVariable10)_file.GetEnumeratorVariable();

                        for (int i = 1; i <= 10; i++)
                        {
                            //Approval and approval date stamp
                            enumVar10.GetVar2(_root_SysName + i.ToString("00"), "", _folder.ID, out retSysName);
                            if (vaultUserName.Equals(retSysName))
                            {
                                enumVar10.SetVar(_root_Vote + i.ToString("00"), "", Stamp_Approval, true);
                                enumVar10.SetVar(_root_VoteDate + i.ToString("00"), "", Stamp_ApprovalDate, true);
                            }

                            //count votes
                            enumVar10.GetVar2(_root_Vote + i.ToString("00"), "", _folder.ID, out RetVote);
                            if (RetVote.Equals(Stamp_Approval))
                            {
                                countVote = countVote + 1;
                            }
                        }
                        enumVar10.CloseFile(false);

                        //If there are more then one approver:
                        if (!(countSysName.Equals(countVote)))
                        {
                            lbl_Report.Text = "updating approval stamp";
                            unlockMessage = "Approved by" + PDM_LoggedInUser.FullName(_vault);
                            ECO_DocumentOptions.Unlock(_vault, _folder, _file, _currentDoc, unlockMessage, form.Handle.ToInt32());


                            if (references.Count() == 0)
                            {

                            }
                            else
                            {
                                lbl_Report.Text = "Unlocking all affected items";
                                PDM_BatchReferences.Batch_Unlock(references, form.Handle.ToInt32());
                            }

                            //Open ECO
                            lbl_Report.Text = "Opening ECO document";
                            ECO_DocumentOptions.Open(_currentDoc);

                            this.Close();

                            //message to user
                            MessageToUser();
                            return;
                        }
                    }
                    #endregion

                    #region (IF) Disapproval - Remove approval stamps
                    lbl_Report.Text = "updating approval stamps";
                    if (ApprvlType == Enum_ApprvlType.Disapproved)
                    {

                        for (int i = 1; i <= 10; i++)
                        {
                            _file = _vault.GetFileFromPath(_currentDoc, out _folder);
                            enumVar10 = (IEdmEnumeratorVariable10)_file.GetEnumeratorVariable();
                            enumVar10.GetVar2(_root_SysName + i.ToString("00"), "", _folder.ID, out retSysName);
                            if (vaultUserName.Equals(retSysName))
                            {

                                enumVar10.SetVar(_root_Vote + i.ToString("00"), "", Stamp_Approval, true); //clear vote
                                enumVar10.SetVar(_root_VoteDate + i.ToString("00"), "", Stamp_ApprovalDate, true); //Clear Root_Vote Date
                                                                                                                   //Dont clear comment
                            }
                            else
                            {
                                //Clear all previous approver stamps
                                enumVar10.SetVar(_root_Vote + i.ToString("00"), "", "", true); //clear vote
                                enumVar10.SetVar(_root_VoteDate + i.ToString("00"), "", "", true); //Clear Root_Vote Date
                                enumVar10.SetVar(_root_Comment + i.ToString("00"), "", "", true); //Clear voter comment  
                            }
                            enumVar10.CloseFile(false);
                        }
                    }
                    #endregion

                    //Unlock all
                    lbl_Report.Text = "Unlocking ECO document";
                    unlockMessage = "Apply notification comment and set transtion switch";
                    ECO_DocumentOptions.Unlock(_vault, _folder, _file, _currentDoc, unlockMessage, form.Handle.ToInt32());

                    if (references.Count() == 0)
                    {
                        lbl_Report.Text = "";
                    }
                    else
                    {
                        lbl_Report.Text = "Unlocking all affected items";
                        PDM_BatchReferences.Batch_Unlock(references, form.Handle.ToInt32());
                    }

                    //Transiton all references             
                    if (references.Count() == 0)
                    {
                        lbl_Report.Text = "";
                    }
                    else
                    {

                        lbl_Report.Text = "Changing status for all affected items";
                        var transName = (from t in db.Transitions
                                         where t.TransitionID.Equals(TransitionID)
                                         select t.Name).First();

                        PDM_BatchReferences.Batch_ChangeState(references, transName, form.Handle.ToInt32());
                    }

                    //Transtion ECO file
                    lbl_Report.Text = "Changing status for ECO document";
                    var stateName = (from s in db.Status
                                     where s.StatusID.Equals(StatusID)
                                     select s.Name).First();

                    ECO_DocumentOptions.ChangeState(_vault, _folder, _file, stateName, _currentDoc, form.Handle.ToInt32());


                    //Apply ECO state label 
                    lbl_Report.Text = "Applying current state label to ECO document";
                    ECO_DocumentOptions.Lock(_vault, _folder, _file, _currentDoc, form.Handle.ToInt32());
                    StateLabel();
                    ECO_DocumentOptions.Unlock(_vault, _folder, _file, _currentDoc, unlockMessage, form.Handle.ToInt32());

                    //Open ECO
                    lbl_Report.Text = "Opening updated ECO document";
                    ECO_DocumentOptions.Open(_currentDoc);


                    //Close form
                    this.Close();

                    //Send message to user
                    MessageToUser();

                    Cursor.Current = Cursors.Default;
                    btn_Cancel.Enabled = true;
                    btn_Clear.Enabled = true;
                    txtBx_Comment.Enabled = true;
                    btn_Submit.Enabled = true;
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

            }
            else
            {
                lbl_Report.Text = "";
                DialogResult dr = MessageBox.Show("Please select one or more approvers", "Blount Change Management", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                this.Close();
            }


        }
        #endregion

        #region Button: Clear Comment
        private void button1_Click(object sender, EventArgs e)
        {
            txtBx_Comment.Clear();
        }
        #endregion

        #region Button: Cancel
        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            //Close form
            btn_Cancel.Enabled = true;
            btn_Clear.Enabled = true;
            txtBx_Comment.Enabled = true;
            btn_Submit.Enabled = true;
            Cursor.Current = Cursors.Default;

            this.Close();
           
        }
        #endregion

        #region Internal Methids
        internal void Comment()
        {
            IEdmEnumeratorVariable10 enumVar10 = enumVar5 as IEdmEnumeratorVariable10;
            string noComment = PDM_LoggedInUser.FullName(_vault) + " did not enter a comment";

            //Clear Sender comments 
            lbl_Report.Text = "Clearing all previous comments";
            _file = _vault.GetFileFromPath(_currentDoc, out _folder);
            enumVar10 = (IEdmEnumeratorVariable10)_file.GetEnumeratorVariable();
            string comment_Sender = SQL_VariableFinder.FetchVariableName((int)Enum_CommentType.Comment_Sender);

            //Clear CCB.Comment_Sender variable
            enumVar10.SetVar(comment_Sender, "", "", true);
            enumVar10.CloseFile(true);

            //(IF) Apply Sender Comment to data card
            if (ApprvlType == Enum_ApprvlType.SendtoApprovers)
            {
                lbl_Report.Text = "Adding comment to card variables";
                _file = _vault.GetFileFromPath(_currentDoc, out _folder);
                enumVar10 = (IEdmEnumeratorVariable10)_file.GetEnumeratorVariable();
                enumVar10.SetVar(comment_Sender, "", "\"" + txtBx_Comment.Text + "\""); //Search for "CCB.Comment_Sender" veriable ID
                enumVar10.CloseFile(true);

                if (string.IsNullOrEmpty(txtBx_Comment.Text))
                {
                    lbl_Report.Text = "No comment entered";
                    _file = _vault.GetFileFromPath(_currentDoc, out _folder);
                    enumVar10 = (IEdmEnumeratorVariable10)_file.GetEnumeratorVariable();
                    enumVar10.SetVar(comment_Sender, "", noComment);
                    enumVar10.CloseFile(true);
                }
            }

            //(IF) Apply Approver/disapprover comment to data card
            lbl_Report.Text = "Adding comments to card variables";
            if (ApprvlType == Enum_ApprvlType.Approved || ApprvlType == Enum_ApprvlType.Disapproved)
            {

                object sysUserName = null;

                //Get current vault user

                string currentUser_SysName = PDM_LoggedInUser.UserName(_vault);

                for (int i = 1; i <= 10; i++)
                {
                    enumVar10 = (IEdmEnumeratorVariable10)_file.GetEnumeratorVariable();
                    enumVar10.GetVar2(_root_SysName + i.ToString("00"), "", _folder.ID, out sysUserName);

                    if (sysUserName.Equals(currentUser_SysName))
                    {
                        if (string.IsNullOrEmpty(txtBx_Comment.Text))
                        {
                            enumVar10.SetVar(_root_Comment + i.ToString("00"), "", noComment);
                        }
                        else
                        {
                            enumVar10.SetVar(_root_Comment + i.ToString("00"), "", "\"" + txtBx_Comment.Text + "\"");
                        }
                    }
                    enumVar10.CloseFile(true);
                }
            }

        }      
        internal void ResetTranSwitch(string fileName)
        {
            try
            {
                lbl_Report.Text = "Resetting all email notifications";
                _file = _vault.GetFileFromPath(fileName, out _folder);
                IEdmEnumeratorVariable10 enumVar10 = enumVar5 as IEdmEnumeratorVariable10;
                enumVar10 = (IEdmEnumeratorVariable10)_file.GetEnumeratorVariable();

                foreach (int enumID in Enum.GetValues(typeof(Enum_TransSwitch)))
                {
                    var varName = (from t in db.Variables
                                   where t.VariableID.Equals(enumID)
                                   select t.VariableName).First();


                    enumVar10.SetVar(varName, "", EdmDataType.EdmData_Nothing, true);

                }
                enumVar10.CloseFile(true);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
        internal void SetTransitionSwitch(string fileName)
        {
            try
            {
                lbl_Report.Text = "Set email notification to current requirments";
                IEdmEnumeratorVariable10 enumVar10 = enumVar5 as IEdmEnumeratorVariable10;
                enumVar10 = (IEdmEnumeratorVariable10)_file.GetEnumeratorVariable();

                var varName = (from t in db.Variables
                               where t.VariableID.Equals(TransSwitch)
                               select t.VariableName).First();
                enumVar10.SetVar(varName, "", 1, true);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        internal void StateLabel()
        {
            try
            {
                IEdmEnumeratorVariable10 enumVar10 = enumVar5 as IEdmEnumeratorVariable10;
                _file = _vault.GetFileFromPath(_currentDoc, out _folder);
                enumVar10 = (IEdmEnumeratorVariable10)_file.GetEnumeratorVariable();
                currentState = _file.CurrentState;
                string fileState = SQL_VariableFinder.FetchVariableName(708); //Variable = CCB.State
                enumVar10.SetVar(fileState, "", currentState.Name);
                enumVar10.CloseFile(true);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        internal void MessageToUser()
        {

            try
            {
                _file = _vault.GetFileFromPath(_currentDoc, out _folder);
                IEdmState5 state = default(IEdmState5);
                state = _file.CurrentState;
                string stateName = state.Name;

                DialogResult dr;
                if (stateName.Equals(SQL_StatusFinder.FetchStatusName(StatusID)))
                {
                    dr = MessageBox.Show(MessageToUser_Success, "Blount Change Management", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    dr = MessageBox.Show(MessageToUser_Failure, "Blount Change Management", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion


    }
}
