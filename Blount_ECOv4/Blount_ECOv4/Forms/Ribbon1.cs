﻿using System;
using Microsoft.Office.Tools.Ribbon;
using System.Windows.Forms; //used for the Save File button
using EPDM.Interop.epdm;
using Microsoft.Office.Interop.Excel;
using Excel = Microsoft.Office.Interop.Excel;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace Blount_ECOv4
{
    public partial class Ribbon1
    {
        #region Initializers
        Form form = new Form();
        IEdmVault5 vault = new EdmVault5();
        IEdmFile5 file = default(IEdmFile5);
        IEdmFolder5 folder = default(IEdmFolder5);
        IEdmState5 state = default(IEdmState5);
        //IEdmEnumeratorVariable5 enumVar5 = default(IEdmEnumeratorVariable5);

        DataClasses1DataContext dB = new DataClasses1DataContext();

        string errorMsg_NamedRange = "Unable to locate the named range for this control.";
        string errorMsg_SelectDivision = "Please select a division before continuing.";
        string transition_Failure = "Transition not successful.Please contact your administrator";
        string DocName = "ECO";
        bool approverFilterRetVal = false; //For interal approval filter method
        #endregion

        #region Ribbon Load and vault login
        private void Ribbon1_Load(object sender, RibbonUIEventArgs e)
        {
            if (vault == null)
            {
                vault = new EdmVault5();
            }
            if (!vault.IsLoggedIn)
            {
                vault.LoginAuto("Blount_vault", form.Handle.ToInt32());
            }
        }
        #endregion

        #region Initialize current doc, Actve doc, and Error messages
        //Gets the full name of this ECO
        internal static string CurrentDoc()
        {
            string currentDoc = Globals.ThisAddIn.Application.ActiveWorkbook.FullName;
            return currentDoc;
        }

        //Gets the active excel sheet that is currently open
        internal static Worksheet ActiveDoc()
        {
            Worksheet currentSheet = Globals.ThisAddIn.GetActiveWorkSheet();
            return currentSheet;
        }

        //Error messages when saving ECO to vault
        internal static void Message(string _message)
        {
            DialogResult dr;
            dr = MessageBox.Show(_message, "Blount Change Management", MessageBoxButtons.OK, MessageBoxIcon.Stop);
        }
        #endregion


        //=======Document========
        #region Button: New ECO (LINQ)
        private void btn_DOC_NewECOdoc_Click(object sender, RibbonControlEventArgs e)
        {
            try
            {
                //Message to user with the option to close current session of Excel
                DialogResult dr = MessageBox.Show("Would you like to close your current session of Excel?", "Blount Change Management", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Information);
                if (dr == DialogResult.Yes)
                {
                    //close current instance of Excel
                    Globals.ThisAddIn.Application.ActiveWorkbook.Close();
                }
                if (dr == DialogResult.Cancel)
                {
                    return;
                }

                //Get ECO Doc ID (LINQ)
                var ecoTemplate = (from d in dB.Documents
                                   where d.DocumentID == 1276625
                                   select d.Filename).First();
                string ECOTemplatePath = vault.RootFolderPath + "\\Templates\\ECO" + "\\" + ecoTemplate;

                //Get Latest ECO template version
                file = vault.GetFileFromPath(ECOTemplatePath, out folder);
                file.GetFileCopy(form.Handle.ToInt32(), 0, 0);

                //Open ECO workbook
                Globals.ThisAddIn.Application.Workbooks.Open(ECOTemplatePath);

                //Get next ECO number and update datebase (LINQ)
                var SerialNo = (from s in dB.SerialNumbers
                                where s.SerialNumberID == 3
                                select s).First();
                SerialNo.Counter = SerialNo.Counter + 1;
                dB.SubmitChanges();

                //Apply ECO number to Excel document
                XLS_RangeData ecoNo = new XLS_RangeData(SerialNo.Counter.ToString());
                ecoNo.SetECOnumber();

                #region Activate buttons
                {
                    //ECO Document
                    btn_DOC_NewECOdoc.Enabled = false;
                    btn_DOC_AffectedLoc.Enabled = true;
                    cbo_DOC_Division.Enabled = true;
                    cbo_DOC_Priority.Enabled = true;
                    cbo_DOC_ECOclass.Enabled = true;
                    cbo_DOC_Disposition.Enabled = false;
                    btn_DOC_AssignECO.Enabled = true;
                    btn_DOC_RequestedBy.Enabled = true;
                    btn_DOC_SelectNotify.Enabled = true;
                    btn_DOC_Save.Enabled = true;
                    btn_DOC_Edit.Enabled = false;
                    btn_DOC_UndoEdit.Enabled = false;

                    //ECO Approval
                    cbo_CCB_Category.Enabled = true;
                    btn_CCB_ApproverTemplate.Enabled = true;
                    btn_CCB_SelectApprover.Enabled = true;

                    //CAD Approval
                    cbo_CAD_Category.Enabled = true;
                    btn_CAD_ApproverTemplate.Enabled = true;
                    btn_CAD_SelectApprover.Enabled = true;

                    //Review
                    btn_REV_OpenLoc.Enabled = true;
                }
                #endregion
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion

        #region Button: Assign ECO
        private void btn_DOC_AssignECO_Click(object sender, RibbonControlEventArgs e)
        {
            Excel.Worksheet activeSheet = (Excel.Worksheet)Globals.ThisAddIn.Application.ActiveSheet;
            if (activeSheet.Name == DocName)
            {
                Form_User user = new Form_User(vault, Enum_FormOption.Assign);
                user.Show();
            }
            else
            {
                MessageBox.Show(errorMsg_NamedRange);
                return;
            }
        }
        #endregion


        #region Load Division and Categories (CCB and CAD) (LINQ)
        string sqlDivision = SQL_VariableFinder.FetchVariableName(710); //CCB.Division


        private void cbo_DOC_Division_ItemsLoading(object sender, RibbonControlEventArgs e)
        {
            Excel.Worksheet activeSheet = (Excel.Worksheet)Globals.ThisAddIn.Application.ActiveSheet;
            if (activeSheet.Name == DocName)
            {
                cbo_DOC_Division.Items.Clear();
                cbo_CAD_Category.Items.Clear();

                try
                {
                    ///Returns unique instances of text before the "/" and all text after the "_" of the Approver.CCB groupName
                    ///Example: "Approvers.CCB_Speeco/Aftermarket Parts" returns "Speeco"

                    var Division = (from g in dB.Groups
                                    where g.Groupname.Contains("Division.BLT")
                                    orderby g.Groupname
                                    select g.Groupname.Remove(g.Groupname.IndexOf('/')).Substring(g.Groupname.IndexOf('_') + 1)).Distinct();

                    foreach (var item in Division)
                    {
                        var rddi = this.Factory.CreateRibbonDropDownItem();
                        rddi.Label = item;
                        cbo_DOC_Division.Items.Add(rddi);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                MessageBox.Show(errorMsg_NamedRange);
                return;
            }
        }
        private void cbo_DOC_Division_TextChanged(object sender, RibbonControlEventArgs e)
        {
            cbo_CCB_Category.Items.Clear();
            cbo_CAD_Category.Items.Clear();

            XLS_RangeData division = new XLS_RangeData(cbo_DOC_Division.Text);
            division.SetDivision();

            //Clear CCB category
            XLS_RangeData category = new XLS_RangeData("");
            category.SetCategory();


            ClearCADAppvlTable();
            ClearCCBAppvlTable();
        }

        private void cbo_CCB_Category_ItemsLoading(object sender, RibbonControlEventArgs e)
        {
            cbo_CCB_Category.Items.Clear();

            Worksheet currentSheet = ActiveDoc();
            string retDivision = currentSheet.Range[sqlDivision].Value;

            if (retDivision == null)
            {
                DialogResult dr = MessageBox.Show(errorMsg_SelectDivision, "Blount Change Management", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            else
            {
                //Load CCB Categories
                //See Database Connection at top of page for connection string
                //Returns text after "/" from the groupnames

                var Category = from g in dB.Groups
                               where g.Groupname.Contains("Approvers.CCB_" + retDivision)
                               orderby g.Groupname
                               select g.Groupname.Substring(g.Groupname.IndexOf('/') + 1);

                foreach (var item in Category)
                {
                    var rddi = this.Factory.CreateRibbonDropDownItem();
                    rddi.Label = item;
                    cbo_CCB_Category.Items.Add(rddi);
                }
            }
        }
        private void cbo_CCB_Category_TextChanged(object sender, RibbonControlEventArgs e)
        {
            cbo_CCB_Category.Items.Clear();

            //Print CCB category
            XLS_RangeData category = new XLS_RangeData(cbo_CCB_Category.Text);
            category.SetCategory();

            ClearCCBAppvlTable();
        }

        private void cbo_CAD_Category_ItemsLoading(object sender, RibbonControlEventArgs e)
        {
            cbo_CAD_Category.Items.Clear();

            Worksheet currentSheet = ActiveDoc();
            string retDivision = currentSheet.Range[sqlDivision].Value;
            if (retDivision == null)
            {
                DialogResult dr = MessageBox.Show(errorMsg_SelectDivision, "Blount Change Management", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            else
            {
                //Load CAD Categories
                var Category0 = from g in dB.Groups
                                where g.Groupname.Contains("Approvers.CAD_" + retDivision)
                                orderby g.Groupname
                                select g.Groupname.Substring(g.Groupname.IndexOf('/') + 1);

                foreach (var item in Category0)
                {
                    var rddi = this.Factory.CreateRibbonDropDownItem();
                    rddi.Label = item;
                    cbo_CAD_Category.Items.Add(rddi);
                }

            }
        }
        private void cbo_CAD_Category_TextChanged(object sender, RibbonControlEventArgs e)
        {
            cbo_CAD_Category.Items.Clear();
            ClearCADAppvlTable();
        }

        internal void ClearCCBAppvlTable()
        {
            Worksheet currentSheet = ActiveDoc();
            try
            {
                for (int i = 1; i <= 10; i++)
                {
                    currentSheet.Range["CCB.Appvl.FullName" + i.ToString("00")].Value = "";
                    currentSheet.Range["CCB.Appvl.Title" + i.ToString("00")].Value = "";
                    currentSheet.Range["CCB.Appvl.SysName" + i.ToString("00")].Value = "";
                    currentSheet.Range["CCB.Vote" + i.ToString("00")].Value = "";
                    currentSheet.Range["CCB.Vote.Date" + i.ToString("00")].Value = "";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        internal void ClearCADAppvlTable()
        {
            Worksheet currentSheet = ActiveDoc();
            try
            {
                for (int i = 1; i <= 10; i++)
                {
                    currentSheet.Range["CAD.Appvl.FullName" + i.ToString("00")].Value = "";
                    currentSheet.Range["CAD.Appvl.Title" + i.ToString("00")].Value = "";
                    currentSheet.Range["CAD.Appvl.SysName" + i.ToString("00")].Value = "";
                    currentSheet.Range["CAD.Vote" + i.ToString("00")].Value = "";
                    currentSheet.Range["CAD.Vote.Date" + i.ToString("00")].Value = "";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion

        #region Load Disposition
        private void cbo_DOC_Disposition_ItemsLoading(object sender, RibbonControlEventArgs e)
        {
            cbo_DOC_Disposition.Items.Clear();

            //Fetch data from database
            var values = from d in dB.CardListValues
                         where d.CardListId.Equals(331)
                         select d.Value;

            foreach (string value in values)
            {
                var rddi = this.Factory.CreateRibbonDropDownItem();
                rddi.Label = value;
                cbo_DOC_Disposition.Items.Add(rddi);
            }
        }

        private void cbo_Disposition_TextChanged(object sender, RibbonControlEventArgs e)
        {
            Worksheet currentSheet = ActiveDoc();
            currentSheet.Range["CCB.Disposition"].Value = cbo_DOC_Disposition.Text;
        }

        #endregion

        #region ComboBox: ECO Class (LINQ)
        private void cbo_DOC_ECOclass_ItemsLoading(object sender, RibbonControlEventArgs e)
        {
            Excel.Worksheet activeSheet = (Excel.Worksheet)Globals.ThisAddIn.Application.ActiveSheet;
            if (activeSheet.Name == DocName)
            {
                //See Database Connection at top of page for connection string
                try
                {
                    cbo_DOC_ECOclass.Items.Clear();
                    var Project = from p in dB.CardListValues
                                  where p.CardListId == 1
                                  select p.Value;

                    foreach (string item in Project)
                    {
                        var rddi = this.Factory.CreateRibbonDropDownItem();
                        rddi.Label = item.ToString();
                        cbo_DOC_ECOclass.Items.Add(rddi);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                MessageBox.Show(errorMsg_NamedRange);
                return;
            }
        }

        private void cbo_DOC_ECOclass_TextChanged(object sender, RibbonControlEventArgs e)
        {
            XLS_RangeData project = new XLS_RangeData(cbo_DOC_ECOclass.Text);
            project.SetProject();
        }
        #endregion

        #region ComboBox: Priority (LINQ)
        private void cbo_DOC_Priority_ItemsLoading(object sender, RibbonControlEventArgs e)
        {
            Excel.Worksheet activeSheet = (Excel.Worksheet)Globals.ThisAddIn.Application.ActiveSheet;
            if (activeSheet.Name == DocName)
            {
                //See Database Connection at top of page for connection string
                try
                {
                    cbo_DOC_Priority.Items.Clear();
                    var Priority = from p in dB.CardListValues
                                   where p.CardListId == 3
                                   select p.Value;

                    foreach (string item in Priority)
                    {
                        //must convert to a format the ribbon can read
                        var rddi = this.Factory.CreateRibbonDropDownItem();
                        rddi.Label = item;
                        cbo_DOC_Priority.Items.Add(rddi);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                MessageBox.Show(errorMsg_NamedRange);
                return;
            }
        }

        private void cbo_DOC_Priority_TextChanged(object sender, RibbonControlEventArgs e)
        {
            XLS_RangeData priority = new XLS_RangeData(cbo_DOC_Priority.Text);
            priority.SetPriority();
        }
        #endregion

        #region Button: Locations
        private void btn_DOC_AffectedLoc_Click_2(object sender, RibbonControlEventArgs e)
        {
            Excel.Worksheet activeSheet = (Excel.Worksheet)Globals.ThisAddIn.Application.ActiveSheet;
            if (activeSheet.Name == DocName)
            {
                form_Location location = new form_Location();
                location.Show();
            }
            else
            {
                MessageBox.Show(errorMsg_NamedRange);
                return;
            }

        }
        #endregion



        #region Button: Requested By
        private void btn_DOC_RequestedBy_Click(object sender, RibbonControlEventArgs e)
        {
            Excel.Worksheet activeSheet = (Excel.Worksheet)Globals.ThisAddIn.Application.ActiveSheet;
            if (activeSheet.Name == DocName)
            {
                Form_User user = new Form_User(vault, Enum_FormOption.Request);
                user.Show();
            }
            else
            {
                MessageBox.Show(errorMsg_NamedRange);
                return;
            }
        }
        #endregion

        #region Button: Select Notification
        private void btn_DOC_SelectNotify_Click(object sender, RibbonControlEventArgs e)
        {
            Excel.Worksheet activeSheet = (Excel.Worksheet)Globals.ThisAddIn.Application.ActiveSheet;
            if (activeSheet.Name == DocName)
            {
                Form_User user = new Form_User(vault, Enum_FormOption.Notify);
                user.Show();
            }
            else
            {
                MessageBox.Show(errorMsg_NamedRange);
                return;
            }
        }
        #endregion


        //=========Save==========
        #region Button: Save
        private void btn_DOC_Save_Click(object sender, RibbonControlEventArgs e)
        {
            try
            {
                Excel.Worksheet activeSheet = (Excel.Worksheet)Globals.ThisAddIn.Application.ActiveSheet;
                if (activeSheet.Name == DocName)
                {
                    #region DataBase Variables
                    //Version check-in
                    string currentDoc = CurrentDoc();
                    file = vault.GetFileFromPath(currentDoc, out folder);
                    state = file.CurrentState;

                    //Get values from the Excel file via the variable ID
                    Worksheet currentSheet = ActiveDoc();
                    string xls_ecoNumber = Convert.ToString(currentSheet.Range[SQL_VariableFinder.FetchVariableName(129)].Value); //ECO Number
                    string xls_priority = currentSheet.Range[SQL_VariableFinder.FetchVariableName((int)Enum_FolderVariables.Priority)].Value;
                    string xls_division = currentSheet.Range[SQL_VariableFinder.FetchVariableName((int)Enum_FolderVariables.Divison)].Value;
                    string xls_description = currentSheet.Range[SQL_VariableFinder.FetchVariableName((int)Enum_FolderVariables.Description)].Value;
                    string xls_category = currentSheet.Range[SQL_VariableFinder.FetchVariableName((int)Enum_FolderVariables.Category)].Value;
                    string xls_assigned = currentSheet.Range[SQL_VariableFinder.FetchVariableName((int)Enum_FolderVariables.AssignedTo)].Value;
                    string xls_ReqstBy = currentSheet.Range[SQL_VariableFinder.FetchVariableName((int)Enum_FolderVariables.RequestBy)].Value;
                    #endregion

                    #region Construct ECO Folder elements
                    string targetEcoFolder = "ECO-" + xls_ecoNumber;
                    string ecoFileName = targetEcoFolder + ".xlsx";

                    string targetRootFolder = vault.RootFolderPath + "\\" + xls_division + "\\ECO\\Active ECO";
                    string localMyDocPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

                    string localFullPath = localMyDocPath + "\\" + ecoFileName;
                    string targetFullPath = targetRootFolder + "\\" + targetEcoFolder + "\\" + ecoFileName;
                    #endregion

                    if (state.ID == (int)Enum_StatusID.ECO_Creation || state.ID == (int)Enum_StatusID.CAD_InDesign)
                    {
                        #region Version Check In
                        try
                        {
                            //Close ECO file
                            Globals.ThisAddIn.Application.DisplayAlerts = false; //dont show the save dialog box
                            Globals.ThisAddIn.Application.ActiveWorkbook.Save();
                            Globals.ThisAddIn.Application.ActiveWorkbook.Close();

                            //Check in and transiton file
                            file.UnlockFile(form.Handle.ToInt32(), "", 0);

                            //Open Vault version
                            Globals.ThisAddIn.Application.Workbooks.Open(currentDoc);

                            //Search and update folder properties
                            FolderProperties(folder, file, xls_ecoNumber, xls_priority, xls_description, xls_description, xls_category, xls_assigned, xls_ReqstBy);

                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                        #endregion
                    }

                    else
                    {
                        #region Initial Check In
                        try
                        {
                            //check if the division is populated if not stop and show error message:
                            if (string.IsNullOrWhiteSpace(xls_division))
                            {
                                Message("Division cannot be empty. Please populate before continuing.");
                                return;
                            }
                            else
                            {
                                //Save Temp file
                                Globals.ThisAddIn.Application.ActiveWorkbook.SaveAs(localFullPath);

                                //Create new ECO vault folder
                                IEdmFolder5 ParrentFodler = default(IEdmFolder5);
                                ParrentFodler = vault.GetFolderFromPath(targetRootFolder);

                                //Check if folder has already been creatred if not, Create new folder if so, use the one found.
                                IEdmFolder5 folder = default(IEdmFolder5);
                                string templateFolder = targetRootFolder + "\\" + targetEcoFolder;
                                if (Directory.Exists(templateFolder))
                                {
                                    MessageBox.Show("Folder already exists in the vault. This ECO will be saved in the matching folder");
                                    folder = vault.GetFolderFromPath(templateFolder);
                                }
                                else
                                {
                                    //Create new ECO folder and add variables to new folder's folder card
                                    folder = ParrentFodler.AddFolder(form.Handle.ToInt32(), targetEcoFolder, null);

                                    //update folder properties
                                    FolderProperties(folder, file, xls_ecoNumber, xls_priority, xls_description, xls_division, xls_category, xls_assigned, xls_ReqstBy);
                                }


                                //Move ECO File from temp location to the new vault
                                int addFileStatus;
                                IEdmFolder8 addFile = (IEdmFolder8)folder;

                                //get vault target folder
                                addFile = (IEdmFolder8)vault.GetFolderFromPath(targetRootFolder + "\\" + targetEcoFolder);

                                //copy file into the new folder
                                addFile.AddFile2(form.Handle.ToInt32(), localFullPath, out addFileStatus, ecoFileName, 0);
                                file = vault.GetFileFromPath(targetFullPath, out folder);

                                //Check in
                                file.UnlockFile(form.Handle.ToInt32(), "Initial Check In", 0, null);
                                folder.Refresh();

                                //Close Excel
                                Globals.ThisAddIn.Application.ActiveWorkbook.Close();

                                // Delete temp file
                                File.Create(localFullPath).Dispose();//terminate the create process
                                File.Delete(localFullPath);//Delete temp file  

                                //Open Vault version in read only
                                file.GetFileCopy(form.Handle.ToInt32(), 0, targetFullPath);
                                file = vault.GetFileFromPath(targetFullPath, out folder);
                                Globals.ThisAddIn.Application.Workbooks.Open(targetFullPath);
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                        #endregion
                    }
                }
                else
                {
                    MessageBox.Show(errorMsg_NamedRange);
                    return;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
        #endregion

        #region Button: Undo Edit
        private void btn_DOC_UndoEdit_Click(object sender, RibbonControlEventArgs e)
        {
            try
            {
                Excel.Worksheet activeSheet = (Excel.Worksheet)Globals.ThisAddIn.Application.ActiveSheet;
                if (activeSheet.Name == DocName)
                {
                    string currentDoc = CurrentDoc();
                    file = vault.GetFileFromPath(currentDoc, out folder);
                    file.GetFileCopy(form.Handle.ToInt32(), "", file.LockPath, 0, "");

                    Globals.ThisAddIn.Application.DisplayAlerts = false;
                    Globals.ThisAddIn.Application.ActiveWorkbook.Close();

                    file.UndoLockFile(form.Handle.ToInt32(), false);
                    Globals.ThisAddIn.Application.Workbooks.Open(currentDoc);

                    btn_DOC_UndoEdit.Enabled = false;

                }
                else
                {
                    MessageBox.Show(errorMsg_NamedRange);
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        #endregion

        #region Button: Edit
        private void btn_DOC_Edit_Click(object sender, RibbonControlEventArgs e)
        {
            Excel.Worksheet activeSheet = (Excel.Worksheet)Globals.ThisAddIn.Application.ActiveSheet;
            if (activeSheet.Name == DocName)
            {
                try
                {
                    string currentDoc = CurrentDoc();
                    file = vault.GetFileFromPath(currentDoc, out folder);

                    //Close ECO file
                    Globals.ThisAddIn.Application.DisplayAlerts = false; //dont show the save dialog box
                    Globals.ThisAddIn.Application.ActiveWorkbook.Close();


                    //Check out file
                    file = vault.GetFileFromPath(currentDoc, out folder);
                    IEdmUser6 currentUser = default(IEdmUser6);
                    if (file.IsLocked)
                    {
                        currentUser = (IEdmUser6)file.LockedByUser;
                        MessageBox.Show("Unable to process your request at this time. " + currentUser.Name + " is currently processing this ECO");
                        return;
                    }
                    else
                    {
                        file.GetFileCopy(form.Handle.ToInt32(), null, folder.ID, 0, "");
                        file.LockFile(folder.ID, form.Handle.ToInt32(), 0);
                    }

                    //Open Vault version
                    Globals.ThisAddIn.Application.Workbooks.Open(currentDoc);


                    //Enable / Disable buttons
                    btn_DOC_Save.Enabled = true;
                    btn_DOC_UndoEdit.Enabled = true;
                    btn_CCB_SendToApprovers.Enabled = false;
                    btn_DOC_Edit.Enabled = false;

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                MessageBox.Show(errorMsg_NamedRange);
                return;
            }

        }
        #endregion


        //=======Approval Templates=========
        #region Approval Template method
        internal void ApproverTemplate(string xlsCell_Division, string ApprvlGroup, string cbo_Category)
        {
            #region Requirments Review
            ///Return Data:
            ///User full name
            ///user system name
            ///User title

            ///Tables
            ///Groupmembers
            ///Groups
            ///Users

            ///Join Keys
            ///GroupsMembers.GroupID to groups.GroupID
            ///GroupMembers.UserID to Users.UserID

            ///Range Variables
            ///gm = GroupMembers
            ///u = Users
            ///g = Groups

            ///Conditions
            ///GroupName = approversMatrix variable
            ///Enabled users only
            #endregion

            Worksheet currentSheet = ActiveDoc();
            string retDivision = currentSheet.Range[xlsCell_Division].Value;

            if (string.IsNullOrWhiteSpace(xlsCell_Division))
            {
                DialogResult dr = MessageBox.Show("Please select a category before continuing", "Blount Change Management", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            else
            {
                try
                {
                    string approversMatrix;
                    if (string.IsNullOrWhiteSpace(cbo_Category)) //if the category cell is empty
                    {
                        approversMatrix = ApprvlGroup + retDivision + "/";
                    }
                    else //If the category cell is populated
                    {
                        approversMatrix = ApprvlGroup + "_" + retDivision + "/" + cbo_Category;
                    }

                    var approvers = from gm in dB.GroupMembers
                                    join u in dB.Users on gm.UserID equals u.UserID
                                    join g in dB.Groups on gm.GroupID equals g.GroupID
                                    where g.Groupname.Equals(approversMatrix)
                                    && u.Enabled == true
                                    select new
                                    {
                                        u.FullName,
                                        u.UserData,
                                        u.Username
                                    };

                    foreach (var approver in approvers)
                    {
                        if (ApprvlGroup.Contains("CCB"))
                        {
                            XLS_RangeData fullName = new XLS_RangeData(approver.FullName);
                            fullName.SetCCBAppvl_FullName();

                            XLS_RangeData title = new XLS_RangeData(approver.UserData);
                            title.SetCCBAppvl_Title();

                            XLS_RangeData sysName = new XLS_RangeData(approver.Username);
                            sysName.SetCCBAppvl_SysName();
                        }

                        if (ApprvlGroup.Contains("CAD"))
                        {
                            XLS_RangeData fullName = new XLS_RangeData(approver.FullName);
                            fullName.SetCADAppvl_FullName();

                            XLS_RangeData title = new XLS_RangeData(approver.UserData);
                            title.SetCADAppvl_Title();

                            XLS_RangeData sysName = new XLS_RangeData(approver.Username);
                            sysName.SetCADAppvlSysName();
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }
        #endregion

        #region Button: CCB Approver Template
        private void btn_CCB_ApproverTemplate_Click(object sender, RibbonControlEventArgs e)
        {
            Excel.Worksheet activeSheet = (Excel.Worksheet)Globals.ThisAddIn.Application.ActiveSheet;
            if (activeSheet.Name == DocName)
            {
                ApproverTemplate("CCB.Division", "Approvers.CCB", cbo_CCB_Category.Text);
            }
            else
            {
                MessageBox.Show(errorMsg_NamedRange);
                return;
            }
        }
        #endregion

        #region Button: CAD Approval Template
        private void btn_CAD_ApproverTemplate_Click(object sender, RibbonControlEventArgs e)
        {
            Excel.Worksheet activeSheet = (Excel.Worksheet)Globals.ThisAddIn.Application.ActiveSheet;
            if (activeSheet.Name == DocName)
            {

                ApproverTemplate("CCB.Division", "Approvers.CAD", cbo_CAD_Category.Text);
            }
            else
            {
                MessageBox.Show(errorMsg_NamedRange);
                return;
            }
        }

        #endregion


        //=======Select Approvers=========
        #region Button: Select ECO Approvers
        private void btn_CCB_SelectApprover_Click(object sender, RibbonControlEventArgs e)
        {
            Excel.Worksheet activeSheet = (Excel.Worksheet)Globals.ThisAddIn.Application.ActiveSheet;
            if (activeSheet.Name == DocName)
            {

                //check if all manditory fields are populated
                Form_User user = new Form_User(vault, Enum_FormOption.ApproveCCB);
                user.Show();
                
            }
            else
            {
                MessageBox.Show(errorMsg_NamedRange);
                return;
            }
        }
        #endregion

        #region Button: Select CAD Approvers
        private void btn_CAD_SelectApprover_Click(object sender, RibbonControlEventArgs e)
        {
            Excel.Worksheet activeSheet = (Excel.Worksheet)Globals.ThisAddIn.Application.ActiveSheet;
            if (activeSheet.Name == DocName)
            {
                Form_User selectUser = new Form_User(vault, Enum_FormOption.ApproveCAD);
                selectUser.Show();
            }
            else
            {
                MessageBox.Show(errorMsg_NamedRange);
                return;
            }
        }
        #endregion

        //======Review Group=====
        #region Browse file location
        private void btn_REV_OpenLoc_Click_3(object sender, RibbonControlEventArgs e)
        {
            try
            {
                string currentDoc = CurrentDoc();
                IEdmFile5 file = default(IEdmFile5);
                IEdmVault8 vault8 = (IEdmVault8)vault;

                file = vault.GetFileFromPath(currentDoc, out folder);
                vault8.OpenContainingFolder(file.LockPath, folder.ID);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion



        //=====Transitions====
        /// <summary>
        /// Transitions data standards:
        /// 1. mark the approval type
        /// 2. mark the transiton ID
        /// 3. Mark the trasition switch
        /// 4. Mark the state(status) ID
        /// 5. Send succeed message to the user.
        /// 6. if not succeed then send the fail message
        /// </summary>

        //=======Cancel=========
        #region CCB Cancel ECO
        private void btn_CCB_Cancel_Click(object sender, RibbonControlEventArgs e)
        {
            Excel.Worksheet activeSheet = (Excel.Worksheet)Globals.ThisAddIn.Application.ActiveSheet;
            if (activeSheet.Name == DocName)
            {
                try
                {
                    string currentDoc = CurrentDoc();
                    Form_TransitionComment process = new Form_TransitionComment(vault, folder, file, currentDoc, "CCB.Appvl.SysName", "CCB.Vote", "CCB.Vote.Date", "CCB.Comment")
                    {
                        ApprvlType = Enum_ApprvlType.Cancel,
                        TransitionID = (int)Enum_Transition.ECO_Cancel,
                        TransSwitch = (int)Enum_TransSwitch.ECO_Cancelled,

                        StatusID = (int)Enum_StatusID.ECO_Cancel,
                        MessageToUser_Success = "ECO has been canceled",
                        MessageToUser_Failure = transition_Failure,
                    };

                    process.ShowDialog();

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                MessageBox.Show(errorMsg_NamedRange);
                return;
            }

        }
        #endregion


        //Send to Approvers=========
        #region Button: Send to ECO Approvers
        private void btn_CCB_SendToApprovers_Click(object sender, RibbonControlEventArgs e)
        {
            Excel.Worksheet activeSheet = (Excel.Worksheet)Globals.ThisAddIn.Application.ActiveSheet;
            if (activeSheet.Name == DocName)
            {
                try
                {
                    string currentDoc = CurrentDoc();
                    string fileName = Path.GetFileName(currentDoc);
                    file = vault.GetFileFromPath(currentDoc, out folder);

                    //and check stop conditions:
                    if (StopCondition(activeSheet) == true)
                    {
                        return;
                    }
                    else
                    {
                        //Check in approvers table contain valid users (by System user name) 
                        DbUserName("Approvers.CCB", "CCB.Appvl.SysName");
                        if (approverFilterRetVal == false)
                        {
                            return;
                        }

                        Form_TransitionComment process = new Form_TransitionComment(vault, folder, file, currentDoc, "CCB.Appvl.SysName", "CCB.Vote", "CCB.Vote.Date", "CCB.Comment")
                        {
                            ApprvlType = Enum_ApprvlType.SendtoApprovers,
                            TransitionID = (int)Enum_Transition.ECO_SendToApprovers,
                            TransSwitch = (int)Enum_TransSwitch.ECO_SendToApprovers,

                            //Feedback message
                            StatusID = (int)Enum_StatusID.ECO_PendingApprvl,
                            MessageToUser_Success = fileName + " has been submitted for approval",
                            MessageToUser_Failure = transition_Failure,

                        };
                        process.ShowDialog();
                    }
                 }
                
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

            }
            else
            {
                MessageBox.Show(errorMsg_NamedRange);
                return;
            }
        }
        #endregion

        #region Button: Send to CAD Approvers
        private void btn_CAD_SendToApprovers_Click(object sender, RibbonControlEventArgs e)
        {
            Excel.Worksheet activeSheet = (Excel.Worksheet)Globals.ThisAddIn.Application.ActiveSheet;
            if (activeSheet.Name == DocName)
            {
                try
                {
                    string currentDoc = CurrentDoc();
                    string fileName = Path.GetFileName(currentDoc);
                    file = vault.GetFileFromPath(currentDoc, out folder);

                    //Check in approvers table contain valid users (by System user name):
                    DbUserName("Approvers.CAD", "CAD.Appvl.SysName");
                    if (approverFilterRetVal == false)
                    {
                        return;
                    }


                    Form_TransitionComment process = new Form_TransitionComment(vault, folder, file, currentDoc, "CAD.Appvl.SysName", "CAD.Vote", "CAD.Vote.Date", "CAD.Comment")
                    {
                        ApprvlType = Enum_ApprvlType.SendtoApprovers,
                        TransSwitch = (int)Enum_TransSwitch.CAD_SendToApprovers,
                        TransitionID = (int)Enum_Transition.CAD_SendToApprovers,

                        //Feedback message
                        StatusID = (int)Enum_StatusID.CAD_PendingApprvl,
                        MessageToUser_Success = fileName + " and all selected references have been submitted for approval",
                        MessageToUser_Failure = transition_Failure,
                    };
                    process.ShowDialog();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                MessageBox.Show(errorMsg_NamedRange);
                return;
            }
        }
        #endregion


        //Approve=========
        #region Button: Approve ECO
        private void btn_CCB_Approve_Click_1(object sender, RibbonControlEventArgs e)
        {

            //Check if Document tab is "ECO"
            Excel.Worksheet activeSheet = (Excel.Worksheet)Globals.ThisAddIn.Application.ActiveSheet;
            string currentDoc = CurrentDoc();
            string fileName = Path.GetFileName(currentDoc);
            file = vault.GetFileFromPath(currentDoc, out folder);

            if (activeSheet.Name == DocName)
            {
                try
                {
                    Form_TransitionComment process = new Form_TransitionComment(vault, folder, file, currentDoc, "CCB.Appvl.SysName", "CCB.Vote", "CCB.Vote.Date", "CCB.Comment")
                    {
                        ApprvlType = Enum_ApprvlType.Approved,
                        TransitionID = (int)Enum_Transition.ECO_Approved,
                        TransSwitch = (int)Enum_TransSwitch.ECO_Approved,
                        Stamp_Approval = "Approved",

                        //Feedback message
                        StatusID = (int)Enum_StatusID.CAD_InDesign,
                        MessageToUser_Success = "Thank you for voting to approve " + fileName + ". You were the final ECO approver. This ECO will now be sent to" + "\"" + SQL_StatusFinder.FetchStatusName((int)Enum_StatusID.CAD_Release) + "\"",
                        MessageToUser_Failure = "Thank you for voting to approve " + fileName + ". Other ECO approvers need to cast their vote before this ECO can be Approved.",
                    };

                    process.ShowDialog();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                MessageBox.Show(errorMsg_NamedRange);
                return;
            }
        }
        #endregion

        #region Button: Approve CAD
        private void btn_CAD_Approved_Click(object sender, RibbonControlEventArgs e)
        {
            //Check if Document tab is "ECO"
            Excel.Worksheet activeSheet = (Excel.Worksheet)Globals.ThisAddIn.Application.ActiveSheet;
            string currentDoc = CurrentDoc();
            //string fileName = Path.GetFileName(currentDoc);
            file = vault.GetFileFromPath(currentDoc, out folder);

            if (activeSheet.Name == DocName)
            {
                try
                {
                    Form_TransitionComment process = new Form_TransitionComment(vault, folder, file, currentDoc, "CAD.Appvl.SysName", "CAD.Vote", "CAD.Vote.Date", "CAD.Comment")
                    {
                        ApprvlType = Enum_ApprvlType.Approved,
                        TransitionID = (int)Enum_Transition.CAD_PendingRelease,
                        TransSwitch = (int)Enum_TransSwitch.CAD_PendingRelease,
                        Stamp_Approval = "Approved",

                        //Feedback message
                        StatusID = (int)Enum_StatusID.CAD_PendingRelease,
                        MessageToUser_Success = "Thank you for your vote",
                        MessageToUser_Failure = "Thank you for your vote",

                        //Cannot use because the release state for FRAG and other files are different states (FRAG is Pending Release while others are released)

                        //MessageToUser_Success = "Thank you for voting to approve the affected items for " + fileName + ". You were the final CAD approver. This ECO will now be sent to " + "\"" + SQL_StatusFinder.FetchStatusName((int)Enum_StatusID.CAD_Release) + "\"",
                        //MessageToUser_Failure = "Thank you for voting to approve the affected items for " + fileName + ". Other CAD approvers need to cast their vote before before affected items can be Approved",
                    };

                    process.ShowDialog();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                MessageBox.Show(errorMsg_NamedRange);
                return;
            }
        }
        #endregion


        //IF_Disapprove=========
        #region Button: Disapprove ECO
        private void btn_CCB_Dispproved_Click(object sender, RibbonControlEventArgs e)
        {
            string currentDoc = CurrentDoc();
            file = vault.GetFileFromPath(currentDoc, out folder);
            string fileName = Path.GetFileName(currentDoc);

            Excel.Worksheet activeSheet = (Excel.Worksheet)Globals.ThisAddIn.Application.ActiveSheet;
            if (activeSheet.Name == DocName)
            {
                try
                {
                    Form_TransitionComment process = new Form_TransitionComment(vault, folder, file, currentDoc, "CCB.Appvl.SysName", "CCB.Vote", "CCB.Vote.Date", "CCB.Comment")
                    {
                        ApprvlType = Enum_ApprvlType.Disapproved,
                        TransitionID = (int)Enum_Transition.ECO_Disapprove,
                        TransSwitch = (int)Enum_TransSwitch.ECO_Disapproved,
                        Stamp_Approval = "Disapproved",

                        //Feedback message
                        StatusID = (int)Enum_StatusID.ECO_Creation,
                        MessageToUser_Success = "Affected items for " + fileName + " were not approved",
                        MessageToUser_Failure = "Transition not successful.Please contact your administrator",
                    };
                    process.ShowDialog();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                MessageBox.Show(errorMsg_NamedRange);
                return;
            }
        }
        #endregion

        #region Button: Disapprove CAD
        private void btn_CAD_Disapprove_Click(object sender, RibbonControlEventArgs e)
        {
            string currentDoc = CurrentDoc();
            file = vault.GetFileFromPath(currentDoc, out folder);
            string fileName = Path.GetFileName(currentDoc);

            Excel.Worksheet activeSheet = (Excel.Worksheet)Globals.ThisAddIn.Application.ActiveSheet;
            if (activeSheet.Name == DocName)
            {
                try
                {
                    Form_TransitionComment process = new Form_TransitionComment(vault, folder, file, currentDoc, "CAD.Appvl.SysName", "CAD.Vote", "CAD.Vote.Date", "CAD.Comment")
                    {
                        ApprvlType = Enum_ApprvlType.Disapproved,
                        TransitionID = (int)Enum_Transition.CAD_Disapproved,
                        TransSwitch = (int)Enum_TransSwitch.CAD_Disapproved,
                        Stamp_Approval = "Disapproved",

                        //Feedback message
                        StatusID = (int)Enum_StatusID.CAD_InDesign,
                        MessageToUser_Success = "Affected items for " + fileName + " were not approved",
                        MessageToUser_Failure = transition_Failure,
                    };
                    process.ShowDialog();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                MessageBox.Show(errorMsg_NamedRange);
                return;
            }
        }







        #endregion

        #region Internal Methods
        //Set Folder propeties
        internal static void FolderProperties(IEdmFolder5 Folder, IEdmFile5 File, string EcoNumber, string Priority, string Description, string Division, string Category, string Assigned, string ReqstBy)
        {

            IEdmEnumeratorVariable5 enumVar5 = default(IEdmEnumeratorVariable5);
            enumVar5 = (IEdmEnumeratorVariable5)File.GetEnumeratorVariable();


            //update folder properties
            enumVar5 = (IEdmEnumeratorVariable5)Folder;
            enumVar5.SetVar(SQL_VariableFinder.FetchVariableName((int)Enum_FolderVariables.ECONumber), "", EcoNumber, true);
            enumVar5.SetVar(SQL_VariableFinder.FetchVariableName((int)Enum_FolderVariables.Priority), "", Priority, true);
            enumVar5.SetVar(SQL_VariableFinder.FetchVariableName((int)Enum_FolderVariables.Description), "", Description, true);
            enumVar5.SetVar(SQL_VariableFinder.FetchVariableName((int)Enum_FolderVariables.AssignedTo), "", Assigned, true);
            enumVar5.SetVar(SQL_VariableFinder.FetchVariableName((int)Enum_FolderVariables.RequestBy), "", ReqstBy, true);
            enumVar5.SetVar(SQL_VariableFinder.FetchVariableName((int)Enum_FolderVariables.Divison), "", Division, true);
            enumVar5.SetVar(SQL_VariableFinder.FetchVariableName((int)Enum_FolderVariables.Category), "", Category, true);
            enumVar5.Flush();

        }

        //Approver filter before submitting for approval
        internal bool DbUserName(string ApproverDBsearchKey, string ApprovalCategory)
        {
            Worksheet currentSheet = ActiveDoc();
            string xlsApproverName = null;
            int xlsCountApprovers = 0;


            //get all approvers from all approvers groups and add them to a list.
            List<string> dbApproverName = new List<string>();
            var dbUserName = (from gm in dB.GroupMembers
                              join u in dB.Users on gm.UserID equals u.UserID
                              join g in dB.Groups on gm.GroupID equals g.GroupID
                              where u.Enabled == true
                              && g.Groupname.StartsWith(ApproverDBsearchKey)
                              orderby u.Username
                              select u.Username).Distinct();

            foreach (string userName in dbUserName)
            {
                dbApproverName.Add(userName);
            }


            List<string> validApprovers = new List<string>();

            //Get all approvers in the approvers table
            for (int xls = 1; xls <= 10; xls++)
            {
                xlsApproverName = currentSheet.Range[ApprovalCategory + xls.ToString("00")].Value;

                if (string.IsNullOrWhiteSpace(xlsApproverName))
                {
                    //Do nothing....
                }
                else
                {
                    //If field is not blank then add a number to the count
                    xlsCountApprovers = xlsCountApprovers + 1;

                    for (int dB = 0; dB < dbApproverName.Count; dB++)
                    {
                        while (xlsApproverName.Equals(dbApproverName[dB].ToString())) //evaluate until false
                        {
                            //add valid approvers to a list to list on the message box of who is able to approve
                            validApprovers.Add(dbApproverName[dB].ToString());
                            break;
                        }
                    }
                }
            }


            //Run Compare to check if the error message is shown
            if (xlsCountApprovers == validApprovers.Count)
            {
                approverFilterRetVal = true;
            }
            else
            {
                StringBuilder sb = new StringBuilder();

                foreach (string validApprover in validApprovers)
                {

                    //Convert userName into FullName :)
                    var fullName = (from u in dB.Users
                                    where u.Username.Equals(validApprover)
                                    select u.FullName).First();

                    sb.AppendLine(fullName);
                }

                DialogResult dr = MessageBox.Show("One or more invalid approvers detected. Valid approvers are:" + Environment.NewLine + Environment.NewLine + sb.ToString() + Environment.NewLine + "Please remove all invalid approvers before continuing", "Blount Change Management", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                approverFilterRetVal = false;
            }
            return approverFilterRetVal;
        }
       
        //check if all manditory fields are populated
        internal static bool StopCondition(Worksheet currentSheet)
        {
            bool stopAction = false;

            string xls_ecoNumber = Convert.ToString(currentSheet.Range[SQL_VariableFinder.FetchVariableName(129)].Value); //ECO Number
            string xls_priority = currentSheet.Range[SQL_VariableFinder.FetchVariableName((int)Enum_FolderVariables.Priority)].Value;
            string xls_description = currentSheet.Range[SQL_VariableFinder.FetchVariableName((int)Enum_FolderVariables.Description)].Value;
            string xls_category = currentSheet.Range[SQL_VariableFinder.FetchVariableName((int)Enum_FolderVariables.Category)].Value;
            string xls_assigned = currentSheet.Range[SQL_VariableFinder.FetchVariableName((int)Enum_FolderVariables.AssignedTo)].Value;
            string xls_ReqstBy = currentSheet.Range[SQL_VariableFinder.FetchVariableName((int)Enum_FolderVariables.RequestBy)].Value;

            if (string.IsNullOrWhiteSpace(xls_ecoNumber))
            {
                Message("ECO Number cannot be empty. Please populate before continuing.");
                stopAction = true;
            }

            if (string.IsNullOrWhiteSpace(xls_priority))
            {
                Message("Please choose the order of priority before continuing.");
                stopAction = true;
            }

            if (string.IsNullOrWhiteSpace(xls_assigned))
            {
                Message("Please assigned this ECO to a team member before continuing.");
                stopAction = true;
            }

            if (string.IsNullOrWhiteSpace(xls_description))
            {
                Message("Please enter a description before continuing");
                stopAction = true;
            }

            if (string.IsNullOrWhiteSpace(xls_category))
            {
                Message("Please select a category before continuing.");
                stopAction = true;
            }

            return stopAction;
        }
        #endregion
    }
}