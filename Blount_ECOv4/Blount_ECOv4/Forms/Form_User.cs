﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Microsoft.Office.Interop.Excel;
using Excel = Microsoft.Office.Interop.Excel;
using EPDM.Interop.epdm;

namespace Blount_ECOv4
{
    public partial class Form_User : Form
    {
        #region constructor and initializers
        DataClasses1DataContext dB = new DataClasses1DataContext(); //LINQ database connection


        string _fullNameSource; //insert from the "Select" buttons
        string LocalFilter = "";
        Enum_FormOption _switchFilter; //enum
        IEdmVault5 _vault;
        

        public Form_User(IEdmVault5 Vault, Enum_FormOption SwitchFilter)
        {
            InitializeComponent();
            this._switchFilter = SwitchFilter;
            this._vault = Vault;
            dataGridView1.BackgroundColor = System.Drawing.Color.Silver;

        }
        #endregion

        #region Selection filter: Set User Option (uses enum)
        public void SetUserOption()
        {
            
            //All users data base query
            var RetData = from u in dB.Users
                          where u.FullName == _fullNameSource
                          && u.Enabled == true
                          select new
                          {
                              u.FullName,
                              u.Username,
                              u.UserData
                          };
          
            switch (_switchFilter) //synce with ribbon for entering users only
            {
                case Enum_FormOption.Assign:

                    #region Initialize Form Header
                    this.Text = "Assign ECO to a Team Member"; //Window Title
                    lbl_Title.Text = "Assign ECO";
                    #endregion

                    #region Assign (LINQ)
                    foreach (var item in RetData)
                    {
                        XLS_RangeData approver = new XLS_RangeData(item.FullName);
                        approver.SetAssignedTo_FullName();

                        XLS_RangeData sysName = new XLS_RangeData(item.Username);
                        sysName.SetAssignedTo_SysName();
                    }
                    break;
                #endregion

                case Enum_FormOption.Request:

                    #region Initial Form Header
                    this.Text = "ECO Requested By"; //Window Title
                    lbl_Title.Text = "ECO Requested By";
                    #endregion

                    #region Request (LINQ)

                    foreach (var item in RetData)
                    {
                        XLS_RangeData approver = new XLS_RangeData(item.FullName);
                        approver.SetRequestedBy_FullName();

                        XLS_RangeData sysName = new XLS_RangeData(item.Username);
                        sysName.SetRequestBy_SysName();
                    }
                    break;
                #endregion

                case Enum_FormOption.ApproveCCB:

                    #region Initial Form Header
                    this.Text = "Select Approver(s) for  this ECO";  //Window Title
                    lbl_Title.Text = "Select ECO Approvers";
                    #endregion

                    #region Approver (LINQ)
                    foreach (var item in RetData)
                    {
                        #region Stop if selecting more then one instance of an approver's system name
                        Worksheet currentSheet = Globals.ThisAddIn.GetActiveWorkSheet();
                        for (int i = 1; i <= 10; i++)
                        {
                            string sysNamecounter = "CCB.Appvl.SysName" + i.ToString("00");
                            string userSyaName = currentSheet.Range[sysNamecounter].Value;

                            if (userSyaName == item.Username)
                            {
                                DialogResult dr = MessageBox.Show("Unable to apply more then one instance of the selected approver", "Blount Change Management", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                return;
                            }
                        }
                        #endregion

                        XLS_RangeData sysName = new XLS_RangeData(item.Username);
                        sysName.SetCCBAppvl_SysName();

                        XLS_RangeData approver = new XLS_RangeData(item.FullName);
                        approver.SetCCBAppvl_FullName();

                        XLS_RangeData title = new XLS_RangeData(item.UserData);
                        title.SetCCBAppvl_Title();


                    }
                    break;
                #endregion

                case Enum_FormOption.ApproveCAD:

                    #region Initial Form Header
                    this.Text = "Select Approvers for CAD documents";
                    lbl_Title.Text = "Select CAD Approvers";
                    #endregion

                    #region Approver (LINQ)
                    foreach (var item in RetData)
                    {
                        //Stop if selecting more then one instance of an approver's system name
                        Worksheet currentSheet = Globals.ThisAddIn.GetActiveWorkSheet();
                        for (int i = 1; i <= 10; i++)
                        {
                            string sysNamecounter = "CAD.Appvl.SysName" + i.ToString("00");
                            string userSyaName = currentSheet.Range[sysNamecounter].Value;

                            if (userSyaName == item.Username)
                            {
                                DialogResult dr = MessageBox.Show("Unable to apply more then one instance of the selected approver", "Blount Change Management", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                return;
                            }
                        }
                        XLS_RangeData fullName = new XLS_RangeData(item.FullName);
                        fullName.SetCADAppvl_FullName();

                        XLS_RangeData sysName = new XLS_RangeData(item.Username);
                        sysName.SetCADAppvlSysName();

                        XLS_RangeData title = new XLS_RangeData(item.UserData);
                        title.SetCADAppvl_Title();
                    }
                    break;
                #endregion

                case Enum_FormOption.Notify:

                    #region Initial Form Header
                    this.Text = "Additional Notifications for this ECO";//Window Title
                    lbl_Title.Text = "Select Additional Notifications";
                    #endregion

                    #region Notify (LINQ)

                    foreach (var item in RetData)
                    {
                        XLS_RangeData approver = new XLS_RangeData(item.FullName);
                        approver.SetNotify_FullName();

                        XLS_RangeData title = new XLS_RangeData(item.UserData);
                        title.SetNotify_Title();

                        XLS_RangeData sysName = new XLS_RangeData(item.Username);
                        sysName.SetNotify_SysName();
                    }
                    break;
                    #endregion
            }
        }
        #endregion

        #region Button: Select Current User (PDM_User Class)
        private void button1_Click(object sender, EventArgs e)
        {
            // assign user's full name to Variable

            _fullNameSource = PDM_LoggedInUser.FullName(_vault);
            SetUserOption();
        }
        #endregion

        #region Form Load ComboBoxes (Search by FullName, Title, Division)
        /// <summary>
        /// Offers the ability to search for all users, or just approvers from CAD or CCB options.
        /// </summary>
        private void Form_SelectUser_Load(object sender, EventArgs e)
        {
            //Name convetion for ECO "Approvers Approvers.CCB_FLAG/Chain"
            //Name convetion for CAD "Approvers Approvers.CAD_FLAG/Chain"
            //Name convetion for Approvers without sub group "Approvers Approvers.CCB_FLAG/"
            //Name convetion for general division groups "Division.BLT_FLAG/
            
            switch (_switchFilter)
            {
                case Enum_FormOption.Assign:
                case Enum_FormOption.Request:
                case Enum_FormOption.Notify:
                    LocalFilter = "Division.BLT";
                    FullName_All(); //Search by Full Name
                    Title_All(); //Search by Title
                    Division_All(LocalFilter); //Search by Division
                    break;

                case Enum_FormOption.ApproveCCB:
                    LocalFilter = "Approvers.CCB";
                    FullName_GroupFilter(LocalFilter); //Search by Full Name
                    Title_GroupFilter(LocalFilter);//Search by Title
                    Division_All(LocalFilter);//Search by Division
                    break;

                case Enum_FormOption.ApproveCAD:
                    LocalFilter = "Approvers.CAD";
                    FullName_GroupFilter(LocalFilter); //Search by Full Name
                    Title_GroupFilter(LocalFilter); //Search by Title
                    Division_All(LocalFilter);//Search by Division
                    break;
            }
 
            SetUserOption();// Initialize Form Header
        }
        #endregion

        #region Button: Select fullName
        private void btn_FullName_Select_Click(object sender, EventArgs e)
        {
            //Set _FullNameSource variable to selected name from the dropdown box

            try
            {
                _fullNameSource = cbo_FullName_List.SelectedItem.ToString();
                SetUserOption();
            }
            catch
            {
                MessageBox.Show("Please make a selection");
            }

        }
        #endregion

        #region Buttton: DataGirdView - Serch by Title (LINQ)
        private void btn_Title_Search_Click(object sender, EventArgs e)
        {
            try
            {
                dataGridView1.BackgroundColor = System.Drawing.Color.White;

                //Reset table
                this.dataGridView1.DataSource = null;
                this.dataGridView1.Rows.Clear();
                this.dataGridView1.Columns.Clear();


                var TitleSearch = from u in dB.Users
                                  where Enabled == true
                                  && u.UserData == cbo_Title_List.Text
                                  && u.UserData != ""
                                  select new
                                  {
                                      Name = u.FullName,
                                      Title = u.UserData
                                  };

                dataGridView1.DataSource = TitleSearch.ToList();
                dataGridView1.AutoResizeColumns();

                //Add "Insert button to last column of table
                DataGridViewButtonColumn btn = new DataGridViewButtonColumn();
                dataGridView1.Columns.Add(btn);
                btn.HeaderText = " ";
                btn.Text = " Insert ";
                btn.Name = "btn_Insert";
                btn.UseColumnTextForButtonValue = true;
                dataGridView1.AutoResizeColumns();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
        #endregion

        #region Button: DataGirdView - Search by Department (LINQ)
        private void button1_Click_1(object sender, EventArgs e)
        {
            ///SEARCH JOIN DATA STRUCTURE
            ///Return: Users's full name and title

            ///Tables: 
            ///Table 1: GroupMembers
            ///Table 2: Groups
            ///Table 3: Users

            ///Join Keys
            ///Join Key: GroupMembers.GoupID on Groups.GroupID
            ///Join Key: GroupMembers.UserID on Users.UserID

            ///Range Variables
            ///gm = GroupMembers
            ///u = users
            ///g = groups

            ///Conditions
            ///Condition 1: UserName dose not start with "%"
            ///Condition 2: Enabled users only
            ///Condition 3: GroupName = "Department - " + cbo_Division_List.SelectedItem

            try
            {
                dataGridView1.BackgroundColor = System.Drawing.Color.White;

                dataGridView1.DataSource = null;
                dataGridView1.Columns.Clear();
                dataGridView1.Rows.Clear();


                if (string.IsNullOrWhiteSpace(cbo_Division_List.Text))
                {
                    MessageBox.Show("Please make a selection");
                }
                else
                {

                    var DivSearch = (from gm in dB.GroupMembers
                                     join g in dB.Groups on gm.GroupID equals g.GroupID
                                     join u in dB.Users on gm.UserID equals u.UserID
                                     where g.Groupname.Contains(LocalFilter + "_" + cbo_Division_List.SelectedItem.ToString() + "/")
                                     && !u.Username.StartsWith("%")
                                     && u.Enabled == true
                                     && u.UserData != ""
                                     select new
                                     {
                                         u.FullName,
                                         u.UserData
                                     }).Distinct();

                    //inser query into dataGridView
                    dataGridView1.DataSource = DivSearch;

                    //add "Insert" Button to dataGridView table
                    DataGridViewButtonColumn btn = new DataGridViewButtonColumn();
                    dataGridView1.Columns.Add(btn);
                    btn.HeaderText = "";
                    btn.Text = " Insert ";
                    btn.Name = "btn_InsertDepartment";
                    btn.UseColumnTextForButtonValue = true;
                    dataGridView1.AutoResizeColumns();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion
   
        #region DataGrid Button: Select
        private void dataGridView1_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {
            //DataGridView button actions

            _fullNameSource = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
            SetUserOption();
        }
        #endregion

        #region Button: Close
        private void btn_Form_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        #region ComboBox dropdown database searches
        //Search by Full Name
        internal string FullName_GroupFilter(string searchFilter)
        {
            var FullName = (from gm in dB.GroupMembers
                                        join u in dB.Users on gm.UserID equals u.UserID
                                        join g in dB.Groups on gm.GroupID equals g.GroupID
                                        where u.Enabled == true
                                        && g.Groupname.Contains(searchFilter)
                                        orderby u.FullName
                                        select u.FullName).Distinct();

            cbo_FullName_List.DataSource = FullName;
            cbo_FullName_List.SelectedIndex = -1;

            return FullName.ToString();
        }
        internal string FullName_All()
        {
            var FullName = from u in dB.Users
                                 where u.Enabled == true
                                 && !u.FullName.StartsWith("!")
                                 && !u.FullName.StartsWith(" ")
                                 && !u.FullName.StartsWith("USPO")
                                 && !u.FullName.Contains("Admin%")
                                 orderby u.FullName
                                 select u.FullName;

            cbo_FullName_List.DataSource = FullName;
            cbo_FullName_List.SelectedIndex = -1;

            return FullName.ToString();
        }
 
        //Search By Title
        internal string Title_GroupFilter(string searchFilter)
        {

            var Title = (from gm in dB.GroupMembers
                         join u in dB.Users on gm.UserID equals u.UserID
                         join g in dB.Groups on gm.GroupID equals g.GroupID
                         where u.Enabled == true
                         && g.Groupname.Contains(searchFilter)
                         orderby u.FullName
                         select u.UserData).Distinct();

           

            cbo_Title_List.DataSource = Title;
            cbo_Title_List.SelectedIndex = -1;

            return Title.ToString();
        }
        internal string Title_All()
        {
            var Title = (from u in dB.Users
                         where u.Enabled == true
                         && !u.UserData.StartsWith(" ")
                         orderby u.UserData
                         select u.UserData).Distinct();

            cbo_Title_List.DataSource = Title;
            cbo_Title_List.SelectedIndex = -1;

            return Title.ToString();
        }

        //Search by Division
        internal string Division_All(string searchFilter)
        {
            //users the DatagridView to search through uers. This just provides the division text.

            var division = (from g in dB.Groups
                            where g.Groupname.Contains(searchFilter)
                            orderby g.Groupname
                            select g.Groupname.Remove(g.Groupname.IndexOf('/')).Substring(g.Groupname.IndexOf('_') + 1)).Distinct();


            cbo_Division_List.DataSource = division;
            cbo_Division_List.SelectedIndex = -1;

            return division.ToString();
        }

        #endregion
    }
}

