﻿namespace Blount_ECOv4
{
    partial class Ribbon1 : Microsoft.Office.Tools.Ribbon.RibbonBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public Ribbon1()
            : base(Globals.Factory.GetRibbonFactory())
        {
            InitializeComponent();
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ECO = this.Factory.CreateRibbonTab();
            this.grp_ECOdoc = this.Factory.CreateRibbonGroup();
            this.btn_DOC_NewECOdoc = this.Factory.CreateRibbonButton();
            this.cbo_DOC_Division = this.Factory.CreateRibbonComboBox();
            this.cbo_DOC_ECOclass = this.Factory.CreateRibbonComboBox();
            this.btn_DOC_AffectedLoc = this.Factory.CreateRibbonButton();
            this.cbo_DOC_Priority = this.Factory.CreateRibbonComboBox();
            this.cbo_DOC_Disposition = this.Factory.CreateRibbonComboBox();
            this.btn_REV_OpenLoc = this.Factory.CreateRibbonButton();
            this.btn_DOC_RequestedBy = this.Factory.CreateRibbonButton();
            this.btn_DOC_AssignECO = this.Factory.CreateRibbonButton();
            this.btn_DOC_SelectNotify = this.Factory.CreateRibbonButton();
            this.grp_AppvlTemplate = this.Factory.CreateRibbonGroup();
            this.cbo_CCB_Category = this.Factory.CreateRibbonComboBox();
            this.btn_CCB_ApproverTemplate = this.Factory.CreateRibbonButton();
            this.btn_CCB_SelectApprover = this.Factory.CreateRibbonButton();
            this.btn_CCB_SendToApprovers = this.Factory.CreateRibbonButton();
            this.btn_CCB_Approved = this.Factory.CreateRibbonButton();
            this.btn_CCB_Dispproved = this.Factory.CreateRibbonButton();
            this.btn_CCB_Cancel = this.Factory.CreateRibbonButton();
            this.group1 = this.Factory.CreateRibbonGroup();
            this.cbo_CAD_Category = this.Factory.CreateRibbonComboBox();
            this.btn_CAD_ApproverTemplate = this.Factory.CreateRibbonButton();
            this.btn_CAD_SelectApprover = this.Factory.CreateRibbonButton();
            this.btn_CAD_SendToApprovers = this.Factory.CreateRibbonButton();
            this.btn_CAD_Approved = this.Factory.CreateRibbonButton();
            this.btn_CAD_Disapprove = this.Factory.CreateRibbonButton();
            this.Save = this.Factory.CreateRibbonGroup();
            this.btn_DOC_Save = this.Factory.CreateRibbonButton();
            this.btn_DOC_Edit = this.Factory.CreateRibbonButton();
            this.btn_DOC_UndoEdit = this.Factory.CreateRibbonButton();
            this.group2 = this.Factory.CreateRibbonGroup();
            this.box3 = this.Factory.CreateRibbonBox();
            this.label1 = this.Factory.CreateRibbonLabel();
            this.lbl_State = this.Factory.CreateRibbonLabel();
            this.box4 = this.Factory.CreateRibbonBox();
            this.label2 = this.Factory.CreateRibbonLabel();
            this.lbl_Owner = this.Factory.CreateRibbonLabel();
            this.ECO.SuspendLayout();
            this.grp_ECOdoc.SuspendLayout();
            this.grp_AppvlTemplate.SuspendLayout();
            this.group1.SuspendLayout();
            this.Save.SuspendLayout();
            this.group2.SuspendLayout();
            this.box3.SuspendLayout();
            this.box4.SuspendLayout();
            this.SuspendLayout();
            // 
            // ECO
            // 
            this.ECO.Groups.Add(this.grp_ECOdoc);
            this.ECO.Groups.Add(this.grp_AppvlTemplate);
            this.ECO.Groups.Add(this.group1);
            this.ECO.Groups.Add(this.Save);
            this.ECO.Groups.Add(this.group2);
            this.ECO.Label = "Change Management";
            this.ECO.Name = "ECO";
            // 
            // grp_ECOdoc
            // 
            this.grp_ECOdoc.Items.Add(this.btn_DOC_NewECOdoc);
            this.grp_ECOdoc.Items.Add(this.cbo_DOC_Division);
            this.grp_ECOdoc.Items.Add(this.cbo_DOC_ECOclass);
            this.grp_ECOdoc.Items.Add(this.btn_DOC_AffectedLoc);
            this.grp_ECOdoc.Items.Add(this.cbo_DOC_Priority);
            this.grp_ECOdoc.Items.Add(this.cbo_DOC_Disposition);
            this.grp_ECOdoc.Items.Add(this.btn_REV_OpenLoc);
            this.grp_ECOdoc.Items.Add(this.btn_DOC_RequestedBy);
            this.grp_ECOdoc.Items.Add(this.btn_DOC_AssignECO);
            this.grp_ECOdoc.Items.Add(this.btn_DOC_SelectNotify);
            this.grp_ECOdoc.Label = "Document";
            this.grp_ECOdoc.Name = "grp_ECOdoc";
            // 
            // btn_DOC_NewECOdoc
            // 
            this.btn_DOC_NewECOdoc.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.btn_DOC_NewECOdoc.Image = global::Blount_ECOv4.Properties.Resources.ECOnumber2;
            this.btn_DOC_NewECOdoc.Label = "New ECO";
            this.btn_DOC_NewECOdoc.Name = "btn_DOC_NewECOdoc";
            this.btn_DOC_NewECOdoc.ShowImage = true;
            this.btn_DOC_NewECOdoc.SuperTip = "This action will open a new ECO template and insert a new ECO number.";
            this.btn_DOC_NewECOdoc.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btn_DOC_NewECOdoc_Click);
            // 
            // cbo_DOC_Division
            // 
            this.cbo_DOC_Division.Enabled = false;
            this.cbo_DOC_Division.Image = global::Blount_ECOv4.Properties.Resources.Division1;
            this.cbo_DOC_Division.Label = "Division";
            this.cbo_DOC_Division.Name = "cbo_DOC_Division";
            this.cbo_DOC_Division.ShowImage = true;
            this.cbo_DOC_Division.SizeString = "Return to this";
            this.cbo_DOC_Division.SuperTip = "Select your division";
            this.cbo_DOC_Division.Text = null;
            this.cbo_DOC_Division.ItemsLoading += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.cbo_DOC_Division_ItemsLoading);
            this.cbo_DOC_Division.TextChanged += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.cbo_DOC_Division_TextChanged);
            // 
            // cbo_DOC_ECOclass
            // 
            this.cbo_DOC_ECOclass.Enabled = false;
            this.cbo_DOC_ECOclass.Image = global::Blount_ECOv4.Properties.Resources.ProjectType6;
            this.cbo_DOC_ECOclass.Label = "ECO Class";
            this.cbo_DOC_ECOclass.Name = "cbo_DOC_ECOclass";
            this.cbo_DOC_ECOclass.ShowImage = true;
            this.cbo_DOC_ECOclass.SizeString = "Return to this";
            this.cbo_DOC_ECOclass.SuperTip = "Select the kind of ECO you would like to create.";
            this.cbo_DOC_ECOclass.Text = null;
            this.cbo_DOC_ECOclass.ItemsLoading += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.cbo_DOC_ECOclass_ItemsLoading);
            this.cbo_DOC_ECOclass.TextChanged += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.cbo_DOC_ECOclass_TextChanged);
            // 
            // btn_DOC_AffectedLoc
            // 
            this.btn_DOC_AffectedLoc.Enabled = false;
            this.btn_DOC_AffectedLoc.Image = global::Blount_ECOv4.Properties.Resources.Location3;
            this.btn_DOC_AffectedLoc.Label = "Add Locations";
            this.btn_DOC_AffectedLoc.Name = "btn_DOC_AffectedLoc";
            this.btn_DOC_AffectedLoc.ShowImage = true;
            this.btn_DOC_AffectedLoc.SuperTip = "Select up to seven locations";
            this.btn_DOC_AffectedLoc.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btn_DOC_AffectedLoc_Click_2);
            // 
            // cbo_DOC_Priority
            // 
            this.cbo_DOC_Priority.Enabled = false;
            this.cbo_DOC_Priority.Image = global::Blount_ECOv4.Properties.Resources.Priority1;
            this.cbo_DOC_Priority.Label = "Priority:";
            this.cbo_DOC_Priority.Name = "cbo_DOC_Priority";
            this.cbo_DOC_Priority.ShowImage = true;
            this.cbo_DOC_Priority.SizeString = "Return to this";
            this.cbo_DOC_Priority.SuperTip = "(Urgent  = 0-2 week) ,(High = 2-3 weeks), (Normal = FIFO) ";
            this.cbo_DOC_Priority.Text = null;
            this.cbo_DOC_Priority.ItemsLoading += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.cbo_DOC_Priority_ItemsLoading);
            this.cbo_DOC_Priority.TextChanged += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.cbo_DOC_Priority_TextChanged);
            // 
            // cbo_DOC_Disposition
            // 
            this.cbo_DOC_Disposition.Enabled = false;
            this.cbo_DOC_Disposition.Image = global::Blount_ECOv4.Properties.Resources.Disposition_48;
            this.cbo_DOC_Disposition.Label = "Disposition";
            this.cbo_DOC_Disposition.Name = "cbo_DOC_Disposition";
            this.cbo_DOC_Disposition.ShowImage = true;
            this.cbo_DOC_Disposition.SizeString = "Return to this";
            this.cbo_DOC_Disposition.Text = null;
            this.cbo_DOC_Disposition.ItemsLoading += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.cbo_DOC_Disposition_ItemsLoading);
            this.cbo_DOC_Disposition.TextChanged += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.cbo_Disposition_TextChanged);
            // 
            // btn_REV_OpenLoc
            // 
            this.btn_REV_OpenLoc.Image = global::Blount_ECOv4.Properties.Resources.Browse_48;
            this.btn_REV_OpenLoc.Label = "File Location";
            this.btn_REV_OpenLoc.Name = "btn_REV_OpenLoc";
            this.btn_REV_OpenLoc.ShowImage = true;
            this.btn_REV_OpenLoc.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btn_REV_OpenLoc_Click_3);
            // 
            // btn_DOC_RequestedBy
            // 
            this.btn_DOC_RequestedBy.Enabled = false;
            this.btn_DOC_RequestedBy.Image = global::Blount_ECOv4.Properties.Resources.RequestedBy;
            this.btn_DOC_RequestedBy.Label = "Requested By";
            this.btn_DOC_RequestedBy.Name = "btn_DOC_RequestedBy";
            this.btn_DOC_RequestedBy.ShowImage = true;
            this.btn_DOC_RequestedBy.SuperTip = "Choose who is requesting this ECO.";
            this.btn_DOC_RequestedBy.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btn_DOC_RequestedBy_Click);
            // 
            // btn_DOC_AssignECO
            // 
            this.btn_DOC_AssignECO.Enabled = false;
            this.btn_DOC_AssignECO.Image = global::Blount_ECOv4.Properties.Resources.AssignedTo;
            this.btn_DOC_AssignECO.Label = "Assign ECO";
            this.btn_DOC_AssignECO.Name = "btn_DOC_AssignECO";
            this.btn_DOC_AssignECO.ShowImage = true;
            this.btn_DOC_AssignECO.SuperTip = "Select the team member who will work on the affected items within this ECO.";
            this.btn_DOC_AssignECO.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btn_DOC_AssignECO_Click);
            // 
            // btn_DOC_SelectNotify
            // 
            this.btn_DOC_SelectNotify.Enabled = false;
            this.btn_DOC_SelectNotify.Image = global::Blount_ECOv4.Properties.Resources.Notifications5;
            this.btn_DOC_SelectNotify.Label = "Notification(s)";
            this.btn_DOC_SelectNotify.Name = "btn_DOC_SelectNotify";
            this.btn_DOC_SelectNotify.ShowImage = true;
            this.btn_DOC_SelectNotify.SuperTip = "Select up to ten additional notifications. This action allows the users in this s" +
    "election to be notified when this ECO is approved or not approved.";
            this.btn_DOC_SelectNotify.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btn_DOC_SelectNotify_Click);
            // 
            // grp_AppvlTemplate
            // 
            this.grp_AppvlTemplate.Items.Add(this.cbo_CCB_Category);
            this.grp_AppvlTemplate.Items.Add(this.btn_CCB_ApproverTemplate);
            this.grp_AppvlTemplate.Items.Add(this.btn_CCB_SelectApprover);
            this.grp_AppvlTemplate.Items.Add(this.btn_CCB_SendToApprovers);
            this.grp_AppvlTemplate.Items.Add(this.btn_CCB_Approved);
            this.grp_AppvlTemplate.Items.Add(this.btn_CCB_Dispproved);
            this.grp_AppvlTemplate.Items.Add(this.btn_CCB_Cancel);
            this.grp_AppvlTemplate.Label = "ECO Approval";
            this.grp_AppvlTemplate.Name = "grp_AppvlTemplate";
            // 
            // cbo_CCB_Category
            // 
            this.cbo_CCB_Category.Enabled = false;
            this.cbo_CCB_Category.Image = global::Blount_ECOv4.Properties.Resources.Catagory;
            this.cbo_CCB_Category.Label = "Category";
            this.cbo_CCB_Category.Name = "cbo_CCB_Category";
            this.cbo_CCB_Category.ShowImage = true;
            this.cbo_CCB_Category.SizeString = "Agriculture";
            this.cbo_CCB_Category.SuperTip = "Select your category. Categories are cascading list boxes that depend on the divi" +
    "sion selected.";
            this.cbo_CCB_Category.Text = null;
            this.cbo_CCB_Category.ItemsLoading += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.cbo_CCB_Category_ItemsLoading);
            this.cbo_CCB_Category.TextChanged += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.cbo_CCB_Category_TextChanged);
            // 
            // btn_CCB_ApproverTemplate
            // 
            this.btn_CCB_ApproverTemplate.Enabled = false;
            this.btn_CCB_ApproverTemplate.Image = global::Blount_ECOv4.Properties.Resources.Template;
            this.btn_CCB_ApproverTemplate.Label = "Approver Template";
            this.btn_CCB_ApproverTemplate.Name = "btn_CCB_ApproverTemplate";
            this.btn_CCB_ApproverTemplate.ShowImage = true;
            this.btn_CCB_ApproverTemplate.SuperTip = "Allows approvers to be saved to a list that enables the automation of populating " +
    "the approvers table. This action’s results depends on the division and category " +
    "combination selected.";
            this.btn_CCB_ApproverTemplate.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btn_CCB_ApproverTemplate_Click);
            // 
            // btn_CCB_SelectApprover
            // 
            this.btn_CCB_SelectApprover.Enabled = false;
            this.btn_CCB_SelectApprover.Image = global::Blount_ECOv4.Properties.Resources.Approver1;
            this.btn_CCB_SelectApprover.Label = "Select Approver";
            this.btn_CCB_SelectApprover.Name = "btn_CCB_SelectApprover";
            this.btn_CCB_SelectApprover.ShowImage = true;
            this.btn_CCB_SelectApprover.SuperTip = "Select up to ten approvers.";
            this.btn_CCB_SelectApprover.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btn_CCB_SelectApprover_Click);
            // 
            // btn_CCB_SendToApprovers
            // 
            this.btn_CCB_SendToApprovers.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.btn_CCB_SendToApprovers.Enabled = false;
            this.btn_CCB_SendToApprovers.Image = global::Blount_ECOv4.Properties.Resources.CheckInandSendToApprovers_48;
            this.btn_CCB_SendToApprovers.Label = "Send to ECO Approvers";
            this.btn_CCB_SendToApprovers.Name = "btn_CCB_SendToApprovers";
            this.btn_CCB_SendToApprovers.ShowImage = true;
            this.btn_CCB_SendToApprovers.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btn_CCB_SendToApprovers_Click);
            // 
            // btn_CCB_Approved
            // 
            this.btn_CCB_Approved.Enabled = false;
            this.btn_CCB_Approved.Image = global::Blount_ECOv4.Properties.Resources.Approve_48;
            this.btn_CCB_Approved.Label = "Approve";
            this.btn_CCB_Approved.Name = "btn_CCB_Approved";
            this.btn_CCB_Approved.ShowImage = true;
            this.btn_CCB_Approved.SuperTip = "You are allowed to approve this ECO if it is in the state \"ECO Waiting for Approv" +
    "al\" and you are listed as an approver";
            this.btn_CCB_Approved.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btn_CCB_Approve_Click_1);
            // 
            // btn_CCB_Dispproved
            // 
            this.btn_CCB_Dispproved.Enabled = false;
            this.btn_CCB_Dispproved.Image = global::Blount_ECOv4.Properties.Resources.Approve_false_48;
            this.btn_CCB_Dispproved.Label = "Disapprove";
            this.btn_CCB_Dispproved.Name = "btn_CCB_Dispproved";
            this.btn_CCB_Dispproved.ShowImage = true;
            this.btn_CCB_Dispproved.SuperTip = "You are allowed to not approve this ECO if it is in the state “ECO Engineering Qu" +
    "eue” and you are listed as an approver";
            this.btn_CCB_Dispproved.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btn_CCB_Dispproved_Click);
            // 
            // btn_CCB_Cancel
            // 
            this.btn_CCB_Cancel.Enabled = false;
            this.btn_CCB_Cancel.Image = global::Blount_ECOv4.Properties.Resources.Cancel_48;
            this.btn_CCB_Cancel.Label = "Cancel";
            this.btn_CCB_Cancel.Name = "btn_CCB_Cancel";
            this.btn_CCB_Cancel.ShowImage = true;
            this.btn_CCB_Cancel.SuperTip = "You are allowed to not cancel this ECO if it is in the state “ECO Engineering Que" +
    "ue” and you are listed as an approver";
            this.btn_CCB_Cancel.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btn_CCB_Cancel_Click);
            // 
            // group1
            // 
            this.group1.Items.Add(this.cbo_CAD_Category);
            this.group1.Items.Add(this.btn_CAD_ApproverTemplate);
            this.group1.Items.Add(this.btn_CAD_SelectApprover);
            this.group1.Items.Add(this.btn_CAD_SendToApprovers);
            this.group1.Items.Add(this.btn_CAD_Approved);
            this.group1.Items.Add(this.btn_CAD_Disapprove);
            this.group1.Label = "Affected Items Approval";
            this.group1.Name = "group1";
            // 
            // cbo_CAD_Category
            // 
            this.cbo_CAD_Category.Enabled = false;
            this.cbo_CAD_Category.Image = global::Blount_ECOv4.Properties.Resources.Catagory;
            this.cbo_CAD_Category.Label = "Category";
            this.cbo_CAD_Category.Name = "cbo_CAD_Category";
            this.cbo_CAD_Category.ShowImage = true;
            this.cbo_CAD_Category.SizeString = "Agriculture";
            this.cbo_CAD_Category.Text = null;
            this.cbo_CAD_Category.ItemsLoading += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.cbo_CAD_Category_ItemsLoading);
            this.cbo_CAD_Category.TextChanged += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.cbo_CAD_Category_TextChanged);
            // 
            // btn_CAD_ApproverTemplate
            // 
            this.btn_CAD_ApproverTemplate.Enabled = false;
            this.btn_CAD_ApproverTemplate.Image = global::Blount_ECOv4.Properties.Resources.ProjectType4;
            this.btn_CAD_ApproverTemplate.Label = "Approver Template";
            this.btn_CAD_ApproverTemplate.Name = "btn_CAD_ApproverTemplate";
            this.btn_CAD_ApproverTemplate.ShowImage = true;
            this.btn_CAD_ApproverTemplate.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btn_CAD_ApproverTemplate_Click);
            // 
            // btn_CAD_SelectApprover
            // 
            this.btn_CAD_SelectApprover.Enabled = false;
            this.btn_CAD_SelectApprover.Image = global::Blount_ECOv4.Properties.Resources.Approver1;
            this.btn_CAD_SelectApprover.Label = "Select Approver";
            this.btn_CAD_SelectApprover.Name = "btn_CAD_SelectApprover";
            this.btn_CAD_SelectApprover.ShowImage = true;
            this.btn_CAD_SelectApprover.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btn_CAD_SelectApprover_Click);
            // 
            // btn_CAD_SendToApprovers
            // 
            this.btn_CAD_SendToApprovers.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.btn_CAD_SendToApprovers.Enabled = false;
            this.btn_CAD_SendToApprovers.Image = global::Blount_ECOv4.Properties.Resources.CheckInandSendToApprovers_48;
            this.btn_CAD_SendToApprovers.Label = "Send to CAD Approvers";
            this.btn_CAD_SendToApprovers.Name = "btn_CAD_SendToApprovers";
            this.btn_CAD_SendToApprovers.ShowImage = true;
            this.btn_CAD_SendToApprovers.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btn_CAD_SendToApprovers_Click);
            // 
            // btn_CAD_Approved
            // 
            this.btn_CAD_Approved.Enabled = false;
            this.btn_CAD_Approved.Image = global::Blount_ECOv4.Properties.Resources.Approve_48;
            this.btn_CAD_Approved.Label = "Approve";
            this.btn_CAD_Approved.Name = "btn_CAD_Approved";
            this.btn_CAD_Approved.ShowImage = true;
            this.btn_CAD_Approved.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btn_CAD_Approved_Click);
            // 
            // btn_CAD_Disapprove
            // 
            this.btn_CAD_Disapprove.Enabled = false;
            this.btn_CAD_Disapprove.Image = global::Blount_ECOv4.Properties.Resources.Approve_false_48;
            this.btn_CAD_Disapprove.Label = "Disapprove";
            this.btn_CAD_Disapprove.Name = "btn_CAD_Disapprove";
            this.btn_CAD_Disapprove.ShowImage = true;
            this.btn_CAD_Disapprove.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btn_CAD_Disapprove_Click);
            // 
            // Save
            // 
            this.Save.Items.Add(this.btn_DOC_Save);
            this.Save.Items.Add(this.btn_DOC_Edit);
            this.Save.Items.Add(this.btn_DOC_UndoEdit);
            this.Save.Label = "Save";
            this.Save.Name = "Save";
            // 
            // btn_DOC_Save
            // 
            this.btn_DOC_Save.Enabled = false;
            this.btn_DOC_Save.Image = global::Blount_ECOv4.Properties.Resources.SaveAs;
            this.btn_DOC_Save.Label = "Save";
            this.btn_DOC_Save.Name = "btn_DOC_Save";
            this.btn_DOC_Save.ShowImage = true;
            this.btn_DOC_Save.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btn_DOC_Save_Click);
            // 
            // btn_DOC_Edit
            // 
            this.btn_DOC_Edit.Enabled = false;
            this.btn_DOC_Edit.Image = global::Blount_ECOv4.Properties.Resources.Edit;
            this.btn_DOC_Edit.Label = "Edit";
            this.btn_DOC_Edit.Name = "btn_DOC_Edit";
            this.btn_DOC_Edit.ShowImage = true;
            this.btn_DOC_Edit.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btn_DOC_Edit_Click);
            // 
            // btn_DOC_UndoEdit
            // 
            this.btn_DOC_UndoEdit.Enabled = false;
            this.btn_DOC_UndoEdit.Image = global::Blount_ECOv4.Properties.Resources.UndoSave_48;
            this.btn_DOC_UndoEdit.Label = "Undo Edit";
            this.btn_DOC_UndoEdit.Name = "btn_DOC_UndoEdit";
            this.btn_DOC_UndoEdit.ShowImage = true;
            this.btn_DOC_UndoEdit.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btn_DOC_UndoEdit_Click);
            // 
            // group2
            // 
            this.group2.Items.Add(this.box3);
            this.group2.Items.Add(this.box4);
            this.group2.Label = "Review";
            this.group2.Name = "group2";
            // 
            // box3
            // 
            this.box3.Items.Add(this.label1);
            this.box3.Items.Add(this.lbl_State);
            this.box3.Name = "box3";
            // 
            // label1
            // 
            this.label1.Label = "State:";
            this.label1.Name = "label1";
            // 
            // lbl_State
            // 
            this.lbl_State.Label = " ";
            this.lbl_State.Name = "lbl_State";
            // 
            // box4
            // 
            this.box4.Items.Add(this.label2);
            this.box4.Items.Add(this.lbl_Owner);
            this.box4.Name = "box4";
            // 
            // label2
            // 
            this.label2.Label = "Owner:";
            this.label2.Name = "label2";
            // 
            // lbl_Owner
            // 
            this.lbl_Owner.Label = " ";
            this.lbl_Owner.Name = "lbl_Owner";
            // 
            // Ribbon1
            // 
            this.Name = "Ribbon1";
            this.RibbonType = "Microsoft.Excel.Workbook";
            this.Tabs.Add(this.ECO);
            this.Load += new Microsoft.Office.Tools.Ribbon.RibbonUIEventHandler(this.Ribbon1_Load);
            this.ECO.ResumeLayout(false);
            this.ECO.PerformLayout();
            this.grp_ECOdoc.ResumeLayout(false);
            this.grp_ECOdoc.PerformLayout();
            this.grp_AppvlTemplate.ResumeLayout(false);
            this.grp_AppvlTemplate.PerformLayout();
            this.group1.ResumeLayout(false);
            this.group1.PerformLayout();
            this.Save.ResumeLayout(false);
            this.Save.PerformLayout();
            this.group2.ResumeLayout(false);
            this.group2.PerformLayout();
            this.box3.ResumeLayout(false);
            this.box3.PerformLayout();
            this.box4.ResumeLayout(false);
            this.box4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btn_DOC_AssignECO;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btn_CCB_SelectApprover;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btn_DOC_SelectNotify;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btn_DOC_Save;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btn_DOC_Edit;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btn_DOC_RequestedBy;
        internal Microsoft.Office.Tools.Ribbon.RibbonComboBox cbo_DOC_Priority;
        internal Microsoft.Office.Tools.Ribbon.RibbonComboBox cbo_DOC_ECOclass;
        internal Microsoft.Office.Tools.Ribbon.RibbonComboBox cbo_DOC_Division;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup grp_ECOdoc;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup grp_AppvlTemplate;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btn_DOC_NewECOdoc;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btn_CCB_ApproverTemplate;
        internal Microsoft.Office.Tools.Ribbon.RibbonTab ECO;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btn_CCB_SendToApprovers;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btn_CCB_Approved;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btn_CCB_Dispproved;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btn_DOC_AffectedLoc;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btn_DOC_UndoEdit;
        internal Microsoft.Office.Tools.Ribbon.RibbonLabel label1;
        internal Microsoft.Office.Tools.Ribbon.RibbonLabel lbl_State;
        internal Microsoft.Office.Tools.Ribbon.RibbonLabel label2;
        internal Microsoft.Office.Tools.Ribbon.RibbonLabel lbl_Owner;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btn_CCB_Cancel;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btn_REV_OpenLoc;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group2;
        internal Microsoft.Office.Tools.Ribbon.RibbonBox box3;
        internal Microsoft.Office.Tools.Ribbon.RibbonBox box4;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group1;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btn_CAD_ApproverTemplate;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btn_CAD_SelectApprover;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btn_CAD_Approved;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btn_CAD_Disapprove;
        internal Microsoft.Office.Tools.Ribbon.RibbonComboBox cbo_CAD_Category;
        internal Microsoft.Office.Tools.Ribbon.RibbonComboBox cbo_CCB_Category;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btn_CAD_SendToApprovers;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup Save;
        internal Microsoft.Office.Tools.Ribbon.RibbonComboBox cbo_DOC_Disposition;
    }

    partial class ThisRibbonCollection
    {
        internal Ribbon1 Ribbon1
        {
            get { return this.GetRibbon<Ribbon1>(); }
        }
    }
}
