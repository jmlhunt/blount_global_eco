﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using Microsoft.Office.Interop.Excel;
using Excel = Microsoft.Office.Interop.Excel;

namespace Blount_ECOv4
{
    public partial class form_Location : Form
    {
        public form_Location()
        {
            InitializeComponent();
        }

        #region Form Load  & test is Location range has been populated
        private void Form_Location_Load(object sender, EventArgs e)
        {
            try
            {
                DataClasses1DataContext dB = new DataClasses1DataContext();
                var Locations = from l in dB.CardListValues
                                where l.CardListId == 327
                                select l.Value;

                foreach (var location in Locations)
                {
                    chkLstBox_Location.Items.Add(location);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            //precheck items already listed in the Excel location range
            Worksheet currentSheet = Globals.ThisAddIn.GetActiveWorkSheet();

            try
            {
                string[] locations = new string[] { currentSheet.Range["CCB.Location01"].Value,
                                                currentSheet.Range["CCB.Location02"].Value,
                                                currentSheet.Range["CCB.Location03"].Value,
                                                currentSheet.Range["CCB.Location04"].Value,
                                                currentSheet.Range["CCB.Location05"].Value,
                                                currentSheet.Range["CCB.Location06"].Value,
                                                currentSheet.Range["CCB.Location07"].Value,
                                                };

                foreach (string location in locations)//Loop through the Excel Location range
                {
                    for (int i = 0; i < chkLstBox_Location.Items.Count; i++) //loop through the checkListBox
                    {
                        if (location == chkLstBox_Location.Items[i].ToString()) //test if text is equal
                        {
                            chkLstBox_Location.SetItemChecked(i, true); //if equal then set items to checked
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion

        #region Button: Insert
        private void button1_Click(object sender, EventArgs e)
        {
            Worksheet currentSheet = Globals.ThisAddIn.GetActiveWorkSheet();

            try
            {
                foreach (string location in chkLstBox_Location.CheckedItems)
                {
                    for (int i = 1; i <= chkLstBox_Location.Items.Count; i++)
                    {
                        if (currentSheet.Range["CCB.Location" + i.ToString("00")].Value == location)
                        {
                            break;
                        }

                        if (string.IsNullOrWhiteSpace(currentSheet.Range["CCB.Location" + i.ToString("00")].Value))
                        {
                            currentSheet.Range["CCB.Location" + i.ToString("00")].Value = location;
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        #endregion

        #region CheckBox: Select All
        private void chkBox_SelectAll_CheckedChanged(object sender, EventArgs e)
        {
            if (chkBox_SelectAll.Checked)
            {
                for (int i = 0; i < chkLstBox_Location.Items.Count; i++)
                {
                    chkLstBox_Location.SetItemChecked(i, true);
                }
            }

            if (!chkBox_SelectAll.Checked)
            {
                for (int i = 0; i < chkLstBox_Location.Items.Count; i++)
                {
                    chkLstBox_Location.SetItemChecked(i, false);
                }
            }
        }
        #endregion

        #region Button: Remove
        private void button2_Click(object sender, EventArgs e)
        {
            Worksheet currentSheet = Globals.ThisAddIn.GetActiveWorkSheet();


            foreach (string location in chkLstBox_Location.CheckedItems)
            {
                for (int i = 1; i <= chkLstBox_Location.Items.Count; i++)
                {
                    if (currentSheet.Range["CCB.Location" + i.ToString("00")].Value == location)
                    {
                        currentSheet.Range["CCB.Location" + i.ToString("00")].Value = "";
                        break;
                    }
                }
            }

        }
        #endregion

        #region Button: Close
        private void btn_Exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
    }
}
