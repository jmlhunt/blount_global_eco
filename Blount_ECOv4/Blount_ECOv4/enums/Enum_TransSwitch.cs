﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blount_ECOv4
{
    //EPDM Database State ID's
    enum Enum_TransSwitch
    {
        CAD_SendToApprovers = 946,
        CAD_Disapproved = 947,
        CAD_PendingRelease = 944,

        ECO_Approved = 701,
        ECO_SendToApprovers = 704,
        ECO_Cancelled = 703,
        ECO_Disapproved = 702,

        CCB_Switch08 = 958, //(In Reserve)
        CCB_Switch09 = 959, //(In Reserve)
        CCB_Switch10 = 960, //(In Reserve)
    }
}
