﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blount_ECOv4
{
    //Workflow State IDs from Status Table in database
    enum Enum_StatusID
    {
              ECO_Creation = 2364,
         ECO_PendingApprvl = 2365,
                ECO_Cancel = 2367,
              CAD_InDesign = 1834,
         CAD_PendingApprvl = 1832,
        CAD_PendingRelease = 2617,
               CAD_Release = 2356
    }
}
