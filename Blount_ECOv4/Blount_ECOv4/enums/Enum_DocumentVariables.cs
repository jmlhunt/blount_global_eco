﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blount_ECOv4
{
    enum Enum_DocumentVariables
    {
        CCB_AssignedToSysName = 686,
        CCB_RequestedBySysName = 713,
        CCB_RequestDate = 769,
        CCB_ReleaseDate = 764,
        CCB_Priority = 765,
        CCB_Division = 710
    }
}
