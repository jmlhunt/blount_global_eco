﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blount_ECOv4
{
    //Variables for creating new ECO folders from the Variable Table in the database
    enum Enum_FolderVariables
    {

        AssignedTo = 712,
        Category = 767,
        RequestBy = 711,
        Priority = 765,   
        Divison = 710,
        Description = 46,    
        ECONumber = 129,   
        
    }
}
