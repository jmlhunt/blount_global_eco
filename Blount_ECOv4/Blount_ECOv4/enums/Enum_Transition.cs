﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blount_ECOv4
{
    enum Enum_Transition
    {
        //Workflow ID = 12
        ECO_SendToApprovers = 3829,
        ECO_Approved = 3843,
        ECO_Disapprove = 3890,
        ECO_Cancel = 3846,

        //Workflow ID = 7
        CAD_SendToApprovers = 1845,
        CAD_PendingRelease = 3506,
        CAD_Disapproved = 1844

    }
}
