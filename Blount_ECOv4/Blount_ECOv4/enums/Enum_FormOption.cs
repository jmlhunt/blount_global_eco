﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blount_ECOv4
{
    //invike the Form_users form when searching for vault users
    public enum Enum_FormOption
    {
        Assign,
        Request,
        ApproveCCB,
        ApproveCAD,
        Notify
    }
}
