﻿using System;
using Excel = Microsoft.Office.Interop.Excel;
using EPDM.Interop.epdm;
using System.Windows.Forms;
using System.Linq;



namespace Blount_ECOv4
{
    public partial class ThisAddIn
    {
        #region variables
        Ribbon1 rbn;

        #endregion

        //runtime
        #region StartUp 
        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
            rbn = Globals.Ribbons.Ribbon1;
            this.Application.WorkbookOpen += new Excel.AppEvents_WorkbookOpenEventHandler(Application_WorkbookOpen);

        }
        #endregion

        #region Application_WorkbookOpen
        /// <summary>
        /// Condition for opening an ECO:
        /// 1. Excel sheet much have a tab labeled as "ECO"
        /// 2. If Excel sheet with ECO tab is not saved in the vault then an error will show and the sheet will close.
        /// 3. if Excel sheet does not contain a ECO tab then Excel will skip this step
        /// </summary>
        void Application_WorkbookOpen(object Sh)
        {
            #region Initializers
            Form form = new Form();
            IEdmVault5 vault = null;
            IEdmState5 state = null;
            IEdmFile5 file = default(IEdmFile5);
            IEdmFolder5 folder = default(IEdmFolder5);
            IEdmEnumeratorVariable5 enumVar5 = default(IEdmEnumeratorVariable5);
            IEdmEnumeratorVariable10 enumVar10 = (IEdmEnumeratorVariable10)enumVar5;
            string currentDoc = Ribbon1.CurrentDoc();
            #endregion

            Excel.Worksheet currentSheet = (Excel.Worksheet)Globals.ThisAddIn.Application.ActiveSheet;
            if (currentSheet.Name == "ECO")
            {
                if (!currentDoc.Contains("Blount_Vault"))
                {
                    DialogResult dr = MessageBox.Show("Selected document is not part of the vault. Please check in before proceeding", "Blount Change Management", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                   //this.Application.ActiveWorkbook.Close(false, missing, missing);
                    return;
                }

                #region log into vault
                if (vault == null)
                {
                    vault = new EdmVault5();
                }

                if (!vault.IsLoggedIn)
                {
                    vault.LoginAuto("Blount_Vault", form.Handle.ToInt32());
                }

                //Get the current logged in user :)
                string vaultUser = PDM_LoggedInUser.UserName(vault);
                #endregion

                #region Reset all buttons back to false
                rbn.btn_DOC_NewECOdoc.Enabled = false;
                rbn.btn_DOC_AssignECO.Enabled = false;
                rbn.cbo_DOC_Division.Enabled = false;
                rbn.cbo_DOC_ECOclass.Enabled = false;
                rbn.cbo_DOC_Priority.Enabled = false;
                rbn.cbo_DOC_Disposition.Enabled = false;
                rbn.btn_DOC_AffectedLoc.Enabled = false;
                rbn.btn_DOC_RequestedBy.Enabled = false;
                rbn.btn_DOC_SelectNotify.Enabled = false;
                rbn.btn_DOC_Save.Enabled = false;
                rbn.btn_DOC_Edit.Enabled = false;
                rbn.btn_DOC_UndoEdit.Enabled = false;
               
                rbn.cbo_CCB_Category.Enabled = false;
                rbn.btn_CCB_ApproverTemplate.Enabled = false;
                rbn.btn_CCB_SelectApprover.Enabled = false;
                rbn.btn_CCB_SendToApprovers.Enabled = false;
                rbn.btn_CCB_Approved.Enabled = false;
                rbn.btn_CCB_Dispproved.Enabled = false;
                rbn.btn_CCB_Cancel.Enabled = false;

                rbn.cbo_CAD_Category.Enabled = false;
                rbn.btn_CAD_ApproverTemplate.Enabled = false;
                rbn.btn_CAD_SelectApprover.Enabled = false;
                rbn.btn_CAD_SendToApprovers.Enabled = false;
                rbn.btn_CAD_Approved.Enabled = false;
                rbn.btn_CAD_Disapprove.Enabled = false;
                #endregion

                #region State: ECO Creation
                file = vault.GetFileFromPath(currentDoc, out folder);
                state = file.CurrentState;
    
                if (state.ID == (int)Enum_StatusID.ECO_Creation)
                {
                    ECOdocEdit(vault, folder, file, state, vaultUser, currentDoc);
                    rbn.btn_CCB_SendToApprovers.Enabled = true;

                    if (file.IsLocked)
                    {
                        rbn.btn_DOC_NewECOdoc.Enabled = false;
                        rbn.btn_DOC_AssignECO.Enabled = true;
                        rbn.cbo_DOC_Division.Enabled = true;
                        rbn.cbo_DOC_ECOclass.Enabled = true;
                        rbn.cbo_DOC_Priority.Enabled = true;
                        rbn.cbo_DOC_Disposition.Enabled = false;
                        rbn.cbo_CAD_Category.Enabled = true;
                        rbn.cbo_CCB_Category.Enabled = true;

                        rbn.btn_DOC_AffectedLoc.Enabled = true;
                        rbn.btn_DOC_RequestedBy.Enabled = true;
                        rbn.btn_DOC_SelectNotify.Enabled = true;
                       
                        rbn.btn_CCB_ApproverTemplate.Enabled = true;
                        rbn.btn_CCB_SelectApprover.Enabled = true;
                        rbn.btn_CCB_SendToApprovers.Enabled = false;
 
                        rbn.btn_CAD_ApproverTemplate.Enabled = true;
                        rbn.btn_CAD_SelectApprover.Enabled = true;
                    }
                        
                }
                #endregion

                #region State: ECO Pending Approval (File cannot be checked out before opening)
                //Scan approval tables for user name and user's vote. If logged in user is on approver's table then activate buttons. If user has already voted the disable Approve/Dispaprove buttons.
                file = vault.GetFileFromPath(currentDoc, out folder);
                if (state.ID == (int)Enum_StatusID.ECO_PendingApprvl)
                {
                    if (file.IsLocked)
                    {
                        rbn.btn_CCB_Approved.Enabled = false;
                        rbn.btn_CCB_Dispproved.Enabled = false;
                        rbn.btn_CCB_Cancel.Enabled = false;
                    }
                    else
                    {
                        for (int i = 1; i <= 10; i++)
                        {
                            string CCB_sysName = "CCB.Appvl.SysName" + i.ToString("00");
                            string CCB_vote = "CCB.Vote" + i.ToString("00");

                            string xlsCCB_SysName = currentSheet.Range[CCB_sysName].Value;
                            string xlsCCB_Vote = currentSheet.Range[CCB_vote].Value;


                            if (vaultUser.Equals(xlsCCB_SysName)) //Scan ECO document
                            {
                                rbn.btn_CCB_Approved.Enabled = true;
                                rbn.btn_CCB_Dispproved.Enabled = true;
                                rbn.btn_CCB_Cancel.Enabled = true;

                                if (xlsCCB_Vote=="Approved" || xlsCCB_Vote=="Disapproved") //If vote is populated then disable
                                {
                                    rbn.btn_CCB_Approved.Enabled = false;
                                    rbn.btn_CCB_Dispproved.Enabled = false;
                                    rbn.btn_CCB_Cancel.Enabled = false;
                                }
                            }
                        }
                    }
                }
                #endregion

                #region State: CAD Pending Approval (File can be checked out before opening)
                if (state.ID.Equals((int)Enum_StatusID.CAD_PendingApprvl))
                {
                    if (file.IsLocked)
                    {
                        rbn.btn_CAD_Approved.Enabled = false;
                        rbn.btn_CAD_Disapprove.Enabled = false;
                    }
                    else
                    {
                        for (int i = 1; i <= 10; i++)
                        {
                            string CAD_sysName = "CAD.Appvl.SysName" + i.ToString("00");
                            string CAD_Vote = "CAD.Vote" + i.ToString("00");

                            string xlsCAD_SysName = currentSheet.Range[CAD_sysName].Value;
                            string xlsCAD_Vote = currentSheet.Range[CAD_Vote].Value;


                            if (vaultUser.Equals(xlsCAD_SysName))
                            {
                                rbn.btn_CAD_Approved.Enabled = true;
                                rbn.btn_CAD_Disapprove.Enabled = true;

                                if (xlsCAD_Vote == "Approved" || xlsCAD_Vote == "Disapproved") //If vote is populated then disable
                                {
                                    rbn.btn_CAD_Approved.Enabled = false;
                                    rbn.btn_CAD_Disapprove.Enabled = false;
                                }
                            }
                        }
                    }
                }
                #endregion

                #region State: In Design (FIle can be checked out before opeing)
                if (state.ID == (int)Enum_StatusID.CAD_InDesign)
                {
                    string sqlAssignedTo = SQL_VariableFinder.FetchVariableName((int)Enum_DocumentVariables.CCB_AssignedToSysName);
                    string xlsAssignedTo = currentSheet.Range[sqlAssignedTo].Value;

                    //Only allow the persion assigned this ECO to ednit and transition document
                    if (string.IsNullOrEmpty(xlsAssignedTo))
                    {
                        MessageBox.Show("This ECO is not assigned to anyone. Please assigne this ECO before continuing");
                    }

                    if (vaultUser.Equals(xlsAssignedTo))
                    {
                        ECOdocEdit(vault, folder, file, state, vaultUser, currentDoc);
                        rbn.btn_CAD_SendToApprovers.Enabled = true;

                        if (file.IsLocked)
                        {
                            rbn.btn_DOC_AffectedLoc.Enabled = true;
                            rbn.cbo_DOC_Disposition.Enabled = false;

                            rbn.cbo_CAD_Category.Enabled = true;
                            rbn.btn_CAD_ApproverTemplate.Enabled = true;
                            rbn.btn_CAD_SelectApprover.Enabled = true;

                            rbn.btn_CAD_SendToApprovers.Enabled = false;
                        }
                    }

                }
                #endregion

                #region Status
                rbn.lbl_State.Label = PDM_Status.SetStatusState(vault, folder, file, state, currentDoc);
                rbn.lbl_Owner.Label = PDM_Status.SetStatusOwner(vault, folder, file, state, currentDoc);
                #endregion
            }
        }
        #endregion

        #region ShutDown
        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
        }
        #endregion

        #region Activate Worksheet
        public Excel.Worksheet GetActiveWorkSheet()
        {
            return (Excel.Worksheet)Application.ActiveSheet;
        }
        #endregion

        #region VSTO generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }

        #endregion

        #region Internal methods
        internal void ECOdocEdit(IEdmVault5 Vault, IEdmFolder5 Folder, IEdmFile5 File, IEdmState5 State, string LoggedUser, string CurrentDoc)
        {

            File = Vault.GetFileFromPath(CurrentDoc, out Folder);
            State = File.CurrentState;

            if (File.IsLocked)
            {
                if (File.LockedByUser.Name == LoggedUser)
                {
                    rbn.btn_DOC_Save.Enabled = true;
                    rbn.btn_DOC_UndoEdit.Enabled = true;
                }
            }
            else
            {
                rbn.btn_DOC_Edit.Enabled = true;
            }
        }
        #endregion
    }
}




